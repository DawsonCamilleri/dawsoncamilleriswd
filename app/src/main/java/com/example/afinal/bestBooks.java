package com.example.afinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class bestBooks extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
//json file
        String JSON_STRING = "{\"employee\":{\"name\":\"Abhishek Saini\",\"salary\":65000}}";

        String JASON = "{\n" +
                "   \"status\":\"OK\",\n" +
                "   \"copyright\":\"Copyright (c) 2019 The New York Times Company.  All Rights Reserved.\",\n" +
                "   \"num_results\":32078,\n" +
                "   \"results\":[  \n" +
                "      {  \n" +
                "         \"title\":\"\\\"I GIVE YOU MY BODY ...\\\"\",\n" +
                "         \"description\":\"The author of the Outlander novels gives tips on writing sex scenes, drawing on examples from the books.\",\n" +
                "         \"contributor\":\"by Diana Gabaldon\",\n" +
                "         \"author\":\"Diana Gabaldon\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Dell\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0399178570\",\n" +
                "               \"isbn13\":\"9780399178573\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0399178570\",\n" +
                "               \"primary_isbn13\":\"9780399178573\",\n" +
                "               \"rank\":8,\n" +
                "               \"list_name\":\"Advice How-To and Miscellaneous\",\n" +
                "               \"display_name\":\"Advice, How-To & Miscellaneous\",\n" +
                "               \"published_date\":\"2016-09-04\",\n" +
                "               \"bestsellers_date\":\"2016-08-20\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"\\\"MOST BLESSED OF THE PATRIARCHS\\\"\",\n" +
                "         \"description\":\"A character study that attempts to make sense of Jefferson\\u2019s contradictions.\",\n" +
                "         \"contributor\":\"by Annette Gordon-Reed and Peter S. Onuf\",\n" +
                "         \"author\":\"Annette Gordon-Reed and Peter S Onuf\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Liveright\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0871404427\",\n" +
                "               \"isbn13\":\"9780871404428\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0871404427\",\n" +
                "               \"primary_isbn13\":\"9780871404428\",\n" +
                "               \"rank\":16,\n" +
                "               \"list_name\":\"Hardcover Nonfiction\",\n" +
                "               \"display_name\":\"Hardcover Nonfiction\",\n" +
                "               \"published_date\":\"2016-05-01\",\n" +
                "               \"bestsellers_date\":\"2016-04-16\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":1,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"#ASKGARYVEE\",\n" +
                "         \"description\":\"The entrepreneur expands on subjects addressed on his Internet show, like marketing, management and social media.\",\n" +
                "         \"contributor\":\"by Gary Vaynerchuk\",\n" +
                "         \"author\":\"Gary Vaynerchuk\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"HarperCollins\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0062273124\",\n" +
                "               \"isbn13\":\"9780062273123\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"isbn10\":\"0062273132\",\n" +
                "               \"isbn13\":\"9780062273130\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0062273124\",\n" +
                "               \"primary_isbn13\":\"9780062273123\",\n" +
                "               \"rank\":5,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2016-04-10\",\n" +
                "               \"bestsellers_date\":\"2016-03-26\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":1\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0062273124\",\n" +
                "               \"primary_isbn13\":\"9780062273123\",\n" +
                "               \"rank\":6,\n" +
                "               \"list_name\":\"Advice How-To and Miscellaneous\",\n" +
                "               \"display_name\":\"Advice, How-To & Miscellaneous\",\n" +
                "               \"published_date\":\"2016-03-27\",\n" +
                "               \"bestsellers_date\":\"2016-03-12\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":1\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"#GIRLBOSS\",\n" +
                "         \"description\":\"An online fashion retailer traces her path to success.\",\n" +
                "         \"contributor\":\"by Sophia Amoruso\",\n" +
                "         \"author\":\"Sophia Amoruso\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Portfolio\\/Penguin\\/Putnam\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"039916927X\",\n" +
                "               \"isbn13\":\"9780399169274\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"isbn10\":\"1591847931\",\n" +
                "               \"isbn13\":\"9781591847939\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"1591847931\",\n" +
                "               \"primary_isbn13\":\"9781591847939\",\n" +
                "               \"rank\":8,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2016-03-13\",\n" +
                "               \"bestsellers_date\":\"2016-02-27\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"1591847931\",\n" +
                "               \"primary_isbn13\":\"9781591847939\",\n" +
                "               \"rank\":9,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2016-01-17\",\n" +
                "               \"bestsellers_date\":\"2016-01-02\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"1591847931\",\n" +
                "               \"primary_isbn13\":\"9781591847939\",\n" +
                "               \"rank\":9,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2015-12-13\",\n" +
                "               \"bestsellers_date\":\"2015-11-28\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"1591847931\",\n" +
                "               \"primary_isbn13\":\"9781591847939\",\n" +
                "               \"rank\":8,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2015-11-15\",\n" +
                "               \"bestsellers_date\":\"2015-10-31\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"039916927X\",\n" +
                "               \"primary_isbn13\":\"9780399169274\",\n" +
                "               \"rank\":10,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2014-11-09\",\n" +
                "               \"bestsellers_date\":\"2014-10-25\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"039916927X\",\n" +
                "               \"primary_isbn13\":\"9780399169274\",\n" +
                "               \"rank\":8,\n" +
                "               \"list_name\":\"Business Books\",\n" +
                "               \"display_name\":\"Business\",\n" +
                "               \"published_date\":\"2014-10-12\",\n" +
                "               \"bestsellers_date\":\"2014-09-27\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"#IMOMSOHARD\",\n" +
                "         \"description\":\"\",\n" +
                "         \"contributor\":\"by Kristin Hensley and Jen Smedley\",\n" +
                "         \"author\":\"Kristin Hensley and Jen Smedley\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"HarperOne\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"006285769X\",\n" +
                "               \"isbn13\":\"9780062857699\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"006285769X\",\n" +
                "               \"primary_isbn13\":\"9780062857699\",\n" +
                "               \"rank\":10,\n" +
                "               \"list_name\":\"Advice How-To and Miscellaneous\",\n" +
                "               \"display_name\":\"Advice, How-To & Miscellaneous\",\n" +
                "               \"published_date\":\"2019-04-21\",\n" +
                "               \"bestsellers_date\":\"2019-04-06\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":1\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"#NEVERAGAIN\",\n" +
                "         \"description\":\"Students from Marjory Stoneman Douglas High School describe the Valentine's Day mass shooting and outline ways to prevent similar incidents.\",\n" +
                "         \"contributor\":\"by David Hogg and Lauren Hogg\",\n" +
                "         \"author\":\"David Hogg and Lauren Hogg\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Random House\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"198480183X\",\n" +
                "               \"isbn13\":\"9781984801838\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"198480183X\",\n" +
                "               \"primary_isbn13\":\"9781984801838\",\n" +
                "               \"rank\":9,\n" +
                "               \"list_name\":\"Paperback Nonfiction\",\n" +
                "               \"display_name\":\"Paperback Nonfiction\",\n" +
                "               \"published_date\":\"2018-07-08\",\n" +
                "               \"bestsellers_date\":\"2018-06-23\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"$100 STARTUP\",\n" +
                "         \"description\":\"How to build a profitable start up for $100 or less and be your own boss.\",\n" +
                "         \"contributor\":\"by Chris Guillebeau\",\n" +
                "         \"author\":\"Chris Guillebeau\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":23,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Crown Business\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0307951529\",\n" +
                "               \"isbn13\":\"9780307951526\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"$20 PER GALLON\",\n" +
                "         \"description\":\"\",\n" +
                "         \"contributor\":\"by Christopher Steiner\",\n" +
                "         \"author\":\"Christopher Steiner\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Grand Central\",\n" +
                "         \"isbns\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'57, Chicago\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"Steve Monroe\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0786867302\",\n" +
                "               \"isbn13\":\"9780786867301\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/2001\\/07\\/29\\/books\\/books-in-brief-fiction-poetry-319660.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'ROCK OF AGES: ''ROLLING STONE'' HISTORY OF ROCK AND ROLL'\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"GEOFFREY STOKES, KEN TUCKER' 'ED WARD\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0671630687\",\n" +
                "               \"isbn13\":\"9780671630683\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/1986\\/12\\/28\\/books\\/three-chord-music-in-a-three-piece-suit.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'THE HIGH ROAD TO CHINA: GEORGE BOGLE, THE PANCHEN LAMA AND THE FIRST BRITISH EXPEDITION TO TIBET'\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"KATE TELTSCHER\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0374217009\",\n" +
                "               \"isbn13\":\"9780374217006\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/2007\\/04\\/22\\/books\\/review\\/Stuart.t.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'TIL DEATH\",\n" +
                "         \"description\":\"\",\n" +
                "         \"contributor\":\"by Sharon Sala\",\n" +
                "         \"author\":\"Sharon Sala\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Harlequin Mira\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0778314278\",\n" +
                "               \"isbn13\":\"9780778314271\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'TIL DEATH DO US PART\",\n" +
                "         \"description\":\"A matchmaker in Victorian England turns to a crime novelist for help when she starts receiving a series of disturbingly personalized trinkets.\",\n" +
                "         \"contributor\":\"by Amanda Quick\",\n" +
                "         \"author\":\"Amanda Quick\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Berkley\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"069819361X\",\n" +
                "               \"isbn13\":\"9780698193611\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"isbn10\":\"039917446X\",\n" +
                "               \"isbn13\":\"9780399174469\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"069819361X\",\n" +
                "               \"primary_isbn13\":\"9780698193611\",\n" +
                "               \"rank\":9,\n" +
                "               \"list_name\":\"E-Book Fiction\",\n" +
                "               \"display_name\":\"E-Book Fiction\",\n" +
                "               \"published_date\":\"2016-05-08\",\n" +
                "               \"bestsellers_date\":\"2016-04-23\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"069819361X\",\n" +
                "               \"primary_isbn13\":\"9780698193611\",\n" +
                "               \"rank\":15,\n" +
                "               \"list_name\":\"Combined Print and E-Book Fiction\",\n" +
                "               \"display_name\":\"Combined Print & E-Book Fiction\",\n" +
                "               \"published_date\":\"2016-05-08\",\n" +
                "               \"bestsellers_date\":\"2016-04-23\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'Til Faith Do Us Part: How Interfaith Marriage is Transforming America\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"Naomi Schaefer Riley\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0199873747\",\n" +
                "               \"isbn13\":\"9780199873746\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/2013\\/05\\/26\\/books\\/review\\/til-faith-do-us-part-by-naomi-schaefer-riley.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"'TIS THE SEASON\",\n" +
                "         \"description\":\"Two classic holiday stories \\u2014 \\\"Under the Christmas Tree\\\" (2009) and \\\"Midnight Confessions\\\" (2010) \\u2014 plus the novella \\\"Backward Glance\\\" (1991).\",\n" +
                "         \"contributor\":\"by Robyn Carr\",\n" +
                "         \"author\":\"Ron Carr\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Harlequin Mira\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0778316645\",\n" +
                "               \"isbn13\":\"9780778316640\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0778316645\",\n" +
                "               \"primary_isbn13\":\"9780778316640\",\n" +
                "               \"rank\":18,\n" +
                "               \"list_name\":\"Mass Market Paperback\",\n" +
                "               \"display_name\":\"Paperback Mass-Market Fiction\",\n" +
                "               \"published_date\":\"2014-11-30\",\n" +
                "               \"bestsellers_date\":\"2014-11-15\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0778316645\",\n" +
                "               \"primary_isbn13\":\"9780778316640\",\n" +
                "               \"rank\":6,\n" +
                "               \"list_name\":\"Mass Market Paperback\",\n" +
                "               \"display_name\":\"Paperback Mass-Market Fiction\",\n" +
                "               \"published_date\":\"2014-11-16\",\n" +
                "               \"bestsellers_date\":\"2014-11-01\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":1,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"------, THAT'S DELICIOUS\",\n" +
                "         \"description\":\"\",\n" +
                "         \"contributor\":\"by Action Bronson with Rachel Wharton\",\n" +
                "         \"author\":\"Action Bronson with Rachel Wharton\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Abrams\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"1419726552\",\n" +
                "               \"isbn13\":\"9781419726552\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"1419726552\",\n" +
                "               \"primary_isbn13\":\"9781419726552\",\n" +
                "               \"rank\":9,\n" +
                "               \"list_name\":\"Advice How-To and Miscellaneous\",\n" +
                "               \"display_name\":\"Advice, How-To & Miscellaneous\",\n" +
                "               \"published_date\":\"2017-10-01\",\n" +
                "               \"bestsellers_date\":\"2017-09-16\",\n" +
                "               \"weeks_on_list\":1,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":1\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"...and the Horse He Rode In On: The People V. Kenneth Starr\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"James Carville\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0684857340\",\n" +
                "               \"isbn13\":\"9780684857343\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/1998\\/10\\/18\\/books\\/preaching-to-the-converted.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\".HACK G.U.   , VOL. 5\",\n" +
                "         \"description\":\"This series, set in the future, is about an online, multiplayer game run amok. This volume concludes the tale.\",\n" +
                "         \"contributor\":\"by Hamazaki Tatsuya\",\n" +
                "         \"author\":\"Hamazaki Tatsuya\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":10.99,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"TOKYOPOP\",\n" +
                "         \"isbns\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"1 Ragged Ridge Road\",\n" +
                "         \"description\":null,\n" +
                "         \"contributor\":null,\n" +
                "         \"author\":\"David Adams Richards\",\n" +
                "         \"contributor_note\":null,\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":null,\n" +
                "         \"publisher\":null,\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0671003542\",\n" +
                "               \"isbn13\":\"9780671003548\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":null,\n" +
                "               \"sunday_review_link\":\"https:\\/\\/www.nytimes.com\\/1997\\/08\\/31\\/books\\/books-in-brief-fiction-747866.html\",\n" +
                "               \"article_chapter_link\":null\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {  \n" +
                "         \"title\":\"1,000 PLACES TO SEE BEFORE YOU DIE\",\n" +
                "         \"description\":\"A guide for traveling the world; second edition updated with new entries.\",\n" +
                "         \"contributor\":\"by Patricia Schultz\",\n" +
                "         \"author\":\"Patricia Schultz\",\n" +
                "         \"contributor_note\":\"\",\n" +
                "         \"price\":0,\n" +
                "         \"age_group\":\"\",\n" +
                "         \"publisher\":\"Workman\",\n" +
                "         \"isbns\":[  \n" +
                "            {  \n" +
                "               \"isbn10\":\"0761156860\",\n" +
                "               \"isbn13\":\"9780761156864\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"isbn10\":\"0761104844\",\n" +
                "               \"isbn13\":\"9780761104841\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"ranks_history\":[  \n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0761156860\",\n" +
                "               \"primary_isbn13\":\"9780761156864\",\n" +
                "               \"rank\":10,\n" +
                "               \"list_name\":\"Travel\",\n" +
                "               \"display_name\":\"Travel\",\n" +
                "               \"published_date\":\"2015-04-12\",\n" +
                "               \"bestsellers_date\":\"2015-03-28\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0761156860\",\n" +
                "               \"primary_isbn13\":\"9780761156864\",\n" +
                "               \"rank\":8,\n" +
                "               \"list_name\":\"Travel\",\n" +
                "               \"display_name\":\"Travel\",\n" +
                "               \"published_date\":\"2015-03-15\",\n" +
                "               \"bestsellers_date\":\"2015-02-28\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            },\n" +
                "            {  \n" +
                "               \"primary_isbn10\":\"0761156860\",\n" +
                "               \"primary_isbn13\":\"9780761156864\",\n" +
                "               \"rank\":12,\n" +
                "               \"list_name\":\"Travel\",\n" +
                "               \"display_name\":\"Travel\",\n" +
                "               \"published_date\":\"2015-01-11\",\n" +
                "               \"bestsellers_date\":\"2014-12-27\",\n" +
                "               \"weeks_on_list\":0,\n" +
                "               \"ranks_last_week\":null,\n" +
                "               \"asterisk\":0,\n" +
                "               \"dagger\":0\n" +
                "            }\n" +
                "         ],\n" +
                "         \"reviews\":[  \n" +
                "            {  \n" +
                "               \"book_review_link\":\"\",\n" +
                "               \"first_chapter_link\":\"\",\n" +
                "               \"sunday_review_link\":\"\",\n" +
                "               \"article_chapter_link\":\"\"\n" +
                "            }\n" +
                "         ]\n" +
                "      }\n" +
                "   ]\n" +
                "}";






        //IIIIIIIIIIIIIIIIIIIIIIIIIIIII













        String Json_string2 = "{  \n" +
                "   \"status\":\"OK\",\n" +
                "   \"copyright\":\"Copyright (c) 2019 The New York Times Company.  All Rights Reserved.\",\n" +
                "   \"num_results\":55,\n" +
                "   \"results\":[  \n" +
                "      {  \n" +
                "         \"list_name\":\"Combined Print and E-Book Fiction\",\n" +
                "         \"display_name\":\"Combined Print & E-Book Fiction\",\n" +
                "         \"list_name_encoded\":\"combined-print-and-e-book-fiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Combined Print and E-Book Nonfiction\",\n" +
                "         \"display_name\":\"Combined Print & E-Book Nonfiction\",\n" +
                "         \"list_name_encoded\":\"combined-print-and-e-book-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Fiction\",\n" +
                "         \"display_name\":\"Hardcover Fiction\",\n" +
                "         \"list_name_encoded\":\"hardcover-fiction\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Nonfiction\",\n" +
                "         \"display_name\":\"Hardcover Nonfiction\",\n" +
                "         \"list_name_encoded\":\"hardcover-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Trade Fiction Paperback\",\n" +
                "         \"display_name\":\"Paperback Trade Fiction\",\n" +
                "         \"list_name_encoded\":\"trade-fiction-paperback\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Mass Market Paperback\",\n" +
                "         \"display_name\":\"Paperback Mass-Market Fiction\",\n" +
                "         \"list_name_encoded\":\"mass-market-paperback\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Paperback Nonfiction\",\n" +
                "         \"display_name\":\"Paperback Nonfiction\",\n" +
                "         \"list_name_encoded\":\"paperback-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"E-Book Fiction\",\n" +
                "         \"display_name\":\"E-Book Fiction\",\n" +
                "         \"list_name_encoded\":\"e-book-fiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"E-Book Nonfiction\",\n" +
                "         \"display_name\":\"E-Book Nonfiction\",\n" +
                "         \"list_name_encoded\":\"e-book-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Advice\",\n" +
                "         \"display_name\":\"Hardcover Advice & Misc.\",\n" +
                "         \"list_name_encoded\":\"hardcover-advice\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2013-04-21\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Paperback Advice\",\n" +
                "         \"display_name\":\"Paperback Advice & Misc.\",\n" +
                "         \"list_name_encoded\":\"paperback-advice\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2013-04-21\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Advice How-To and Miscellaneous\",\n" +
                "         \"display_name\":\"Advice, How-To & Miscellaneous\",\n" +
                "         \"list_name_encoded\":\"advice-how-to-and-miscellaneous\",\n" +
                "         \"oldest_published_date\":\"2013-04-28\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Chapter Books\",\n" +
                "         \"display_name\":\"Children\\u2019s Chapter Books\",\n" +
                "         \"list_name_encoded\":\"chapter-books\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2012-12-09\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Childrens Middle Grade\",\n" +
                "         \"display_name\":\"Children\\u2019s Middle Grade\",\n" +
                "         \"list_name_encoded\":\"childrens-middle-grade\",\n" +
                "         \"oldest_published_date\":\"2012-12-16\",\n" +
                "         \"newest_published_date\":\"2015-08-23\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Childrens Middle Grade E-Book\",\n" +
                "         \"display_name\":\"Children\\u2019s Middle Grade E-Book\",\n" +
                "         \"list_name_encoded\":\"childrens-middle-grade-e-book\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Childrens Middle Grade Hardcover\",\n" +
                "         \"display_name\":\"Children\\u2019s Middle Grade Hardcover\",\n" +
                "         \"list_name_encoded\":\"childrens-middle-grade-hardcover\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Childrens Middle Grade Paperback\",\n" +
                "         \"display_name\":\"Children\\u2019s Middle Grade Paperback\",\n" +
                "         \"list_name_encoded\":\"childrens-middle-grade-paperback\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Paperback Books\",\n" +
                "         \"display_name\":\"Children\\u2019s Paperback Books\",\n" +
                "         \"list_name_encoded\":\"paperback-books\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2012-12-09\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Picture Books\",\n" +
                "         \"display_name\":\"Children\\u2019s Picture Books\",\n" +
                "         \"list_name_encoded\":\"picture-books\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Series Books\",\n" +
                "         \"display_name\":\"Children\\u2019s Series\",\n" +
                "         \"list_name_encoded\":\"series-books\",\n" +
                "         \"oldest_published_date\":\"2008-06-08\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Young Adult\",\n" +
                "         \"display_name\":\"Young Adult\",\n" +
                "         \"list_name_encoded\":\"young-adult\",\n" +
                "         \"oldest_published_date\":\"2012-12-16\",\n" +
                "         \"newest_published_date\":\"2015-08-23\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Young Adult E-Book\",\n" +
                "         \"display_name\":\"Young Adult E-Book\",\n" +
                "         \"list_name_encoded\":\"young-adult-e-book\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Young Adult Hardcover\",\n" +
                "         \"display_name\":\"Young Adult Hardcover\",\n" +
                "         \"list_name_encoded\":\"young-adult-hardcover\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2019-05-19\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Young Adult Paperback\",\n" +
                "         \"display_name\":\"Young Adult Paperback\",\n" +
                "         \"list_name_encoded\":\"young-adult-paperback\",\n" +
                "         \"oldest_published_date\":\"2015-08-30\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Graphic Books\",\n" +
                "         \"display_name\":\"Hardcover Graphic Books\",\n" +
                "         \"list_name_encoded\":\"hardcover-graphic-books\",\n" +
                "         \"oldest_published_date\":\"2009-03-15\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Paperback Graphic Books\",\n" +
                "         \"display_name\":\"Paperback Graphic Books\",\n" +
                "         \"list_name_encoded\":\"paperback-graphic-books\",\n" +
                "         \"oldest_published_date\":\"2009-03-15\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Manga\",\n" +
                "         \"display_name\":\"Manga\",\n" +
                "         \"list_name_encoded\":\"manga\",\n" +
                "         \"oldest_published_date\":\"2009-03-15\",\n" +
                "         \"newest_published_date\":\"2017-01-29\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Combined Print Fiction\",\n" +
                "         \"display_name\":\"Combined Hardcover & Paperback Fiction\",\n" +
                "         \"list_name_encoded\":\"combined-print-fiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2013-05-12\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Combined Print Nonfiction\",\n" +
                "         \"display_name\":\"Combined Hardcover & Paperback Nonfiction\",\n" +
                "         \"list_name_encoded\":\"combined-print-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2011-02-13\",\n" +
                "         \"newest_published_date\":\"2013-05-12\",\n" +
                "         \"updated\":\"WEEKLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Animals\",\n" +
                "         \"display_name\":\"Animals\",\n" +
                "         \"list_name_encoded\":\"animals\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Audio Fiction\",\n" +
                "         \"display_name\":\"Audio Fiction\",\n" +
                "         \"list_name_encoded\":\"audio-fiction\",\n" +
                "         \"oldest_published_date\":\"2018-03-11\",\n" +
                "         \"newest_published_date\":\"2019-05-12\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Audio Nonfiction\",\n" +
                "         \"display_name\":\"Audio Nonfiction\",\n" +
                "         \"list_name_encoded\":\"audio-nonfiction\",\n" +
                "         \"oldest_published_date\":\"2018-03-11\",\n" +
                "         \"newest_published_date\":\"2019-05-12\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Business Books\",\n" +
                "         \"display_name\":\"Business\",\n" +
                "         \"list_name_encoded\":\"business-books\",\n" +
                "         \"oldest_published_date\":\"2013-11-03\",\n" +
                "         \"newest_published_date\":\"2019-05-12\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Celebrities\",\n" +
                "         \"display_name\":\"Celebrities\",\n" +
                "         \"list_name_encoded\":\"celebrities\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Crime and Punishment\",\n" +
                "         \"display_name\":\"Crime and Punishment\",\n" +
                "         \"list_name_encoded\":\"crime-and-punishment\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Culture\",\n" +
                "         \"display_name\":\"Culture\",\n" +
                "         \"list_name_encoded\":\"culture\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Education\",\n" +
                "         \"display_name\":\"Education\",\n" +
                "         \"list_name_encoded\":\"education\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Espionage\",\n" +
                "         \"display_name\":\"Espionage\",\n" +
                "         \"list_name_encoded\":\"espionage\",\n" +
                "         \"oldest_published_date\":\"2014-12-14\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Expeditions Disasters and Adventures\",\n" +
                "         \"display_name\":\"Expeditions\",\n" +
                "         \"list_name_encoded\":\"expeditions-disasters-and-adventures\",\n" +
                "         \"oldest_published_date\":\"2014-12-14\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Fashion Manners and Customs\",\n" +
                "         \"display_name\":\"Fashion, Manners and Customs\",\n" +
                "         \"list_name_encoded\":\"fashion-manners-and-customs\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Food and Fitness\",\n" +
                "         \"display_name\":\"Food and Diet\",\n" +
                "         \"list_name_encoded\":\"food-and-fitness\",\n" +
                "         \"oldest_published_date\":\"2013-09-01\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Games and Activities\",\n" +
                "         \"display_name\":\"Games and Activities\",\n" +
                "         \"list_name_encoded\":\"games-and-activities\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Business Books\",\n" +
                "         \"display_name\":\"Hardcover Business Books\",\n" +
                "         \"list_name_encoded\":\"hardcover-business-books\",\n" +
                "         \"oldest_published_date\":\"2011-07-03\",\n" +
                "         \"newest_published_date\":\"2013-10-13\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Health\",\n" +
                "         \"display_name\":\"Health\",\n" +
                "         \"list_name_encoded\":\"health\",\n" +
                "         \"oldest_published_date\":\"2014-10-12\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Humor\",\n" +
                "         \"display_name\":\"Humor\",\n" +
                "         \"list_name_encoded\":\"humor\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Indigenous Americans\",\n" +
                "         \"display_name\":\"Indigenous Americans\",\n" +
                "         \"list_name_encoded\":\"indigenous-americans\",\n" +
                "         \"oldest_published_date\":\"2014-12-14\",\n" +
                "         \"newest_published_date\":\"2016-01-10\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Relationships\",\n" +
                "         \"display_name\":\"Love and Relationships\",\n" +
                "         \"list_name_encoded\":\"relationships\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Paperback Business Books\",\n" +
                "         \"display_name\":\"Paperback Business Books\",\n" +
                "         \"list_name_encoded\":\"paperback-business-books\",\n" +
                "         \"oldest_published_date\":\"2011-07-03\",\n" +
                "         \"newest_published_date\":\"2013-10-13\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Family\",\n" +
                "         \"display_name\":\"Parenthood and Family\",\n" +
                "         \"list_name_encoded\":\"family\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Hardcover Political Books\",\n" +
                "         \"display_name\":\"Politics and American History\",\n" +
                "         \"list_name_encoded\":\"hardcover-political-books\",\n" +
                "         \"oldest_published_date\":\"2011-07-03\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Race and Civil Rights\",\n" +
                "         \"display_name\":\"Race and Civil Rights\",\n" +
                "         \"list_name_encoded\":\"race-and-civil-rights\",\n" +
                "         \"oldest_published_date\":\"2014-12-14\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Religion Spirituality and Faith\",\n" +
                "         \"display_name\":\"Religion, Spirituality and Faith\",\n" +
                "         \"list_name_encoded\":\"religion-spirituality-and-faith\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Science\",\n" +
                "         \"display_name\":\"Science\",\n" +
                "         \"list_name_encoded\":\"science\",\n" +
                "         \"oldest_published_date\":\"2013-04-14\",\n" +
                "         \"newest_published_date\":\"2019-05-12\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Sports\",\n" +
                "         \"display_name\":\"Sports and Fitness\",\n" +
                "         \"list_name_encoded\":\"sports\",\n" +
                "         \"oldest_published_date\":\"2014-03-02\",\n" +
                "         \"newest_published_date\":\"2019-05-12\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      },\n" +
                "      {  \n" +
                "         \"list_name\":\"Travel\",\n" +
                "         \"display_name\":\"Travel\",\n" +
                "         \"list_name_encoded\":\"travel\",\n" +
                "         \"oldest_published_date\":\"2014-09-07\",\n" +
                "         \"newest_published_date\":\"2017-01-15\",\n" +
                "         \"updated\":\"MONTHLY\"\n" +
                "      }\n" +
                "   ]\n" +
                "}";


        String name, salary;
        TextView employeeName;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);


            setContentView(R.layout.bestbooks);


            //   setContentView(activity2);


                aw();

            // get the reference of TextView's
            employeeName = (TextView) findViewById(R.id.name2);
            String data ="";
            String dataParsed = "";
            String singleParsed ="";
            try {


                // get JSONObject from JSON file
                // JSONObject obj = new JSONObject(Json_string2);
                // fetch JSONObject named employee
                // JSONObject employee = obj.getJSONObject("results");
                // get employee name and salary
                //  name = employee.getString("list_name");
                // set employee name and salary in TextView's
                //  employeeName.setText("Name: " + name);



                JSONObject jsonObj = new JSONObject(JASON);
                String p = "";String p2 = " ";
                JSONArray c = jsonObj.getJSONArray("results");

                for (int i = 1 ; i < c.length(); i++) {
                    JSONObject obj = c.getJSONObject(i);
                    String A = obj.getString("title");
                    String B = obj.getString("description");
                    String C = "";
                    //   p +=  "\n \n Name:\n    " + A + "\n \n" ;
                    //   p2 +=  "\n \n \n Publish Date:\n  " + B + "\n \n ";


                    JSONArray jArray = obj.getJSONArray("ranks_history");
                    for (int j = 0; j < jArray.length(); j++)
                    {
                        JSONObject jOBJNEW = jArray.getJSONObject(j);
                        C = jOBJNEW.getString("rank");

                    }


                    p +=  " \nName:\n" + A + "\n "  + "Description:\n" +  B + "\n" + "Rank:\n " + C + "\n";

                }
                employeeName.setText(p);


                //   employeeName.setText(p);
                // employeeSalary.setText(p2);



                //JSONArray JA = new JSONArray(Json_string2);
                //   for(int i =0 ;i <JA.length(); i++){
                //      JSONObject JO = (JSONObject) JA.get(i);
                //       singleParsed =  "Name:" + JO.get("list_name");


                //    dataParsed = dataParsed + singleParsed +"\n" ;


                //   }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            // employeeName.setText("H " + dataParsed);

        }













    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            // setTitle("#");
            //  first p = new first();
            // FragmentManager frag = getSupportFragmentManager();
            //   frag.beginTransaction().replace(R.id.fragment,p).commit();
            Intent intentMain = new Intent(bestBooks.this,
                    MainActivity.class);
            bestBooks.this.startActivity(intentMain);


        } else if (id == R.id.nav_gallery) {

            Intent intentMain = new Intent(bestBooks.this,
                    MostSharedArticles.class);
            bestBooks.this.startActivity(intentMain);

        } else if (id == R.id.nav_slideshow) {
            Intent intentMain = new Intent(bestBooks.this,
                    mostemailedarticles.class);
            bestBooks.this.startActivity(intentMain);

        } else if (id == R.id.nav_tools) {
            Intent intentMain = new Intent(bestBooks.this,
                    mostviewedarticles.class);
            bestBooks.this.startActivity(intentMain);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (id == R.id.share) {
            Intent intentMain = new Intent(bestBooks.this,
                    search.class);
            bestBooks.this.startActivity(intentMain);


        }

        return true;

    }

    private void aw() {

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Menu menu = navigationView.getMenu();

        MenuItem gallery = menu.findItem(R.id.nav_gallery);
        MenuItem slideshow = menu.findItem(R.id.nav_slideshow);
        MenuItem tools = menu.findItem(R.id.nav_tools);
        MenuItem books = menu.findItem(R.id.bestbooks);


        // set new title to the MenuItem
        gallery.setTitle("Most Emailed Articles");
        slideshow.setTitle("Most Shared Articles");
        tools.setTitle("Most Viewed Articles");
        books.setTitle("Best Sellers Articles");


        MenuItem search = menu.findItem(R.id.share);

        search.setTitle("search books");

    }

    }



