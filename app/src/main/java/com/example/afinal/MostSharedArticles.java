package com.example.afinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MostSharedArticles extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private Toolbar toolbar;
    SharedPreferences sp;
    public static final String KEY_NAME = "link";
    private SharedPreferences.Editor mEditor;

    String JASON = "{  \n" +
            "   \"status\":\"OK\",\n" +
            "   \"copyright\":\"Copyright (c) 2019 The New York Times Company.  All Rights Reserved.\",\n" +
            "   \"num_results\":1103,\n" +
            "   \"results\":[  \n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/us\\/abortion-law-alabama.html\",\n" +
            "         \"adx_keywords\":\"Abortion;Alabama;Roe v Wade (Supreme Court Decision);Alabama Pro-Life Coalition;Law and Legislation;State Legislatures;Women's Rights;Supreme Court (US);Johnston, Eric (Attorney)\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":1,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006501413,\n" +
            "         \"asset_id\":100000006501413,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By TIMOTHY WILLIAMS and ALAN BLINDER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Lawmakers Vote to Effectively Ban Abortion in Alabama\",\n" +
            "         \"abstract\":\"Supporters of the measure hope it will lead the Supreme Court to reconsider Roe v. Wade, which recognized a constitutional right to end a pregnancy.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 18:00:13\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"LAW AND LEGISLATION\",\n" +
            "            \"STATE LEGISLATURES\",\n" +
            "            \"WOMEN'S RIGHTS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\",\n" +
            "            \"ALABAMA PRO-LIFE COALITION\",\n" +
            "            \"SUPREME COURT (US)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"JOHNSTON, ERIC (ATTORNEY)\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Demonstrators dressed as handmaids protested at the Alabama State House in April against the proposed abortion ban.\",\n" +
            "               \"copyright\":\"Mickey Welsh\\/The Montgomery Advertiser, via Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/38f3c228-7789-5f4b-88f0-6bb61c99599e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/opinion\\/trump-willmar-minnesota.html\",\n" +
            "         \"adx_keywords\":\"Willmar (Minn);Immigration and Emigration;Somali-Americans;Hispanic-Americans;Calvin, Marv;Trump, Donald J;Labor and Jobs;Education (K-12)\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":2,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006507843,\n" +
            "         \"asset_id\":100000006507843,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By THOMAS L. FRIEDMAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"President Trump, Come to Willmar\",\n" +
            "         \"abstract\":\"This Minnesota town is a modern, successful American melting pot.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 20:59:54\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOMALI-AMERICANS\",\n" +
            "            \"EDUCATION (K-12)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"IMMIGRATION AND EMIGRATION\",\n" +
            "            \"HISPANIC-AMERICANS\",\n" +
            "            \"LABOR AND JOBS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"CALVIN, MARV\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"WILLMAR (MINN)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Faiza Dalmar Awil crossing the street between her restaurant, Somali Star, and her shop in Willmar, Minn.\",\n" +
            "               \"copyright\":\"Jenn Ackerman for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/7fd2805e-7e30-5ec2-9a76-2560d4d4ed3f\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/style\\/generation-xers.html\",\n" +
            "         \"adx_keywords\":\"Music;Pop and Rock Music;Television;Fashion and Apparel;Magazines;Smoking and Tobacco;Lollapalooza (Music Festival);Nineteen Hundred Nineties;Benetton Group SpA;Dr Martens;Drug Abuse Resistance Education (DARE);Living Colour (Music Group);Parents Music Resource Center;TLC (Music Group);Bonet, Lisa;Cobain, Kurt;Gore, Tipper;Kravitz, Lenny;Reeves, Keanu;Prada, Miuccia;Twin Peaks (TV Program);Generation X\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":3,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":\"\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"id\":100000006490125,\n" +
            "         \"asset_id\":100000006490125,\n" +
            "         \"nytdsection\":\"style\",\n" +
            "         \"byline\":\"\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Gen X Is a Mess\",\n" +
            "         \"abstract\":\"The great generation that barely was.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 21:36:56\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"MUSIC\",\n" +
            "            \"POP AND ROCK MUSIC\",\n" +
            "            \"TELEVISION\",\n" +
            "            \"GENERATION X\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"FASHION AND APPAREL\",\n" +
            "            \"MAGAZINES\",\n" +
            "            \"SMOKING AND TOBACCO\",\n" +
            "            \"LOLLAPALOOZA (MUSIC FESTIVAL)\",\n" +
            "            \"NINETEEN HUNDRED NINETIES\",\n" +
            "            \"BENETTON GROUP SPA\",\n" +
            "            \"DR MARTENS\",\n" +
            "            \"DRUG ABUSE RESISTANCE EDUCATION (DARE)\",\n" +
            "            \"LIVING COLOUR (MUSIC GROUP)\",\n" +
            "            \"PARENTS MUSIC RESOURCE CENTER\",\n" +
            "            \"TLC (MUSIC GROUP)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BONET, LISA\",\n" +
            "            \"COBAIN, KURT\",\n" +
            "            \"GORE, TIPPER\",\n" +
            "            \"KRAVITZ, LENNY\",\n" +
            "            \"REEVES, KEANU\",\n" +
            "            \"PRADA, MIUCCIA\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Lenny Kravitz and Lisa Bonet\",\n" +
            "               \"copyright\":\"Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/f2717793-1a5c-581f-8d8c-0a2dde2e2dcc\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/us\\/alabama-abortion-facts-law-bill.html\",\n" +
            "         \"adx_keywords\":\"Law and Legislation;Abortion;Alabama;Roe v Wade (Supreme Court Decision);Women's Rights;State Legislatures;Supreme Court (US);Republican Party;Ivey, Kay\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":4,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006510512,\n" +
            "         \"asset_id\":100000006510512,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By ALAN BLINDER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Alabama Governor Signs Abortion Bill. Here\\u2019s What Comes Next\",\n" +
            "         \"abstract\":\"The Alabama governor signed the bill into law Wednesday afternoon. But the last word will probably come from the federal courts.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 02:14:55\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"LAW AND LEGISLATION\",\n" +
            "            \"ABORTION\",\n" +
            "            \"WOMEN'S RIGHTS\",\n" +
            "            \"STATE LEGISLATURES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\",\n" +
            "            \"SUPREME COURT (US)\",\n" +
            "            \"REPUBLICAN PARTY\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"IVEY, KAY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\\u00a0\",\n" +
            "               \"copyright\":\"Christopher Aluka Berry\\/Reuters\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/14ABORTION-hpfader-slide-8N51\\/14ABORTION-hpfader-slide-8N51-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/14ABORTION-hpfader-slide-8N51\\/14ABORTION-hpfader-slide-8N51-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/14ABORTION-hpfader-slide-8N51\\/14ABORTION-hpfader-slide-8N51-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b5913907-95e5-5465-824c-57ac07e9287c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/11\\/us\\/politics\\/democrats-house-oath.html\",\n" +
            "         \"adx_keywords\":\"United States Politics and Government;Constitution (US);Religion-State Relations;Democratic Party;House of Representatives;Politics and Government;Religion and Belief;Freedom of Religion;Cohen, Stephen I;Nadler, Jerrold;Cheney, Liz;Duncan, Jeffrey D\",\n" +
            "         \"subsection\":\"politics\",\n" +
            "         \"share_count\":5,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006441397,\n" +
            "         \"asset_id\":100000006441397,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By CATIE EDMONDSON\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"\\u2018So Help Me God\\u2019 No More: Democrats Give House Traditions a Makeover\",\n" +
            "         \"abstract\":\"The Democrats running the House are changing things, including sometimes omitting words from the swearing-in oath, and some Republicans are not pleased.\",\n" +
            "         \"published_date\":\"2019-05-11\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-12 04:37:38\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"POLITICS AND GOVERNMENT\",\n" +
            "            \"RELIGION AND BELIEF\",\n" +
            "            \"FREEDOM OF RELIGION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"CONSTITUTION (US)\",\n" +
            "            \"RELIGION-STATE RELATIONS\",\n" +
            "            \"DEMOCRATIC PARTY\",\n" +
            "            \"HOUSE OF REPRESENTATIVES\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"COHEN, STEPHEN I\",\n" +
            "            \"NADLER, JERROLD\",\n" +
            "            \"CHENEY, LIZ\",\n" +
            "            \"DUNCAN, JEFFREY D\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Mark Makela for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/us\\/11dc-oath-promo\\/11dc-oath-promo-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/us\\/11dc-oath-promo\\/merlin_154534311_88fd13f7-dc57-40f2-abe6-7c3b5ca40ab2-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/us\\/11dc-oath-promo\\/merlin_154534311_88fd13f7-dc57-40f2-abe6-7c3b5ca40ab2-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/d60d059e-e4a2-5989-ba1e-abf42ad1d997\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/12\\/opinion\\/nike-maternity-leave.html\",\n" +
            "         \"adx_keywords\":\"NIKE Inc;Pregnancy and Childbirth;Track and Field;Athletics and Sports;Paid Time Off;Discrimination;Advertising and Marketing;maternity leave;parental leave;Running;Goucher, Kara;Freelancing, Self-Employment and Independent Contracting;Montano, Alysia (1986- );your-feed-opinionvideo\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":6,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006473770,\n" +
            "         \"asset_id\":100000006473770,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By ALYSIA MONTA\\u00d1O, MAX CANTOR, TAIGE JENSEN and LINDSAY CROUSE\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Nike Told Me to Dream Crazy, Until I Wanted a Baby\",\n" +
            "         \"abstract\":\"Being a mother and a champion was a crazy dream. It didn\\u2019t have to be.\",\n" +
            "         \"published_date\":\"2019-05-12\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 18:38:17\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"PREGNANCY AND CHILDBIRTH\",\n" +
            "            \"ATHLETICS AND SPORTS\",\n" +
            "            \"PAID TIME OFF\",\n" +
            "            \"DISCRIMINATION\",\n" +
            "            \"RUNNING\",\n" +
            "            \"FREELANCING, SELF-EMPLOYMENT AND INDEPENDENT CONTRACTING\",\n" +
            "            \"YOUR-FEED-OPINIONVIDEO\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"NIKE INC\",\n" +
            "            \"TRACK AND FIELD\",\n" +
            "            \"ADVERTISING AND MARKETING\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"GOUCHER, KARA\",\n" +
            "            \"MONTANO, ALYSIA (1986- )\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/autossell\\/op-nike-pregnancy\\/op-nike-pregnancy-thumbStandard-v3.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/autossell\\/op-nike-pregnancy\\/op-nike-pregnancy-mediumThreeByTwo210-v3.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/autossell\\/op-nike-pregnancy\\/op-nike-pregnancy-mediumThreeByTwo440-v3.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/918721b2-a1bb-5882-97f0-80e1022d743d\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/style\\/busy-philipps-abortion-youknowme.html\",\n" +
            "         \"adx_keywords\":\"Abortion;Philipps, Busy;Alabama;Busy Tonight (TV Program);Television;Social Media\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":7,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"id\":100000006511207,\n" +
            "         \"asset_id\":100000006511207,\n" +
            "         \"nytdsection\":\"style\",\n" +
            "         \"byline\":\"By VALERIYA SAFRONOVA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Thousands of Women Have Shared Abortion Stories With #YouKnowMe. She Was First.\",\n" +
            "         \"abstract\":\"After Busy Philipps opened up about her abortion on TV, a friend saw an opportunity for a bigger conversation about reproductive rights.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 17:07:38\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"TELEVISION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"SOCIAL MEDIA\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"PHILIPPS, BUSY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Busy Philipps in 2018. The actress spoke about her abortion, at age 15, on her late-night show, \\u201cBusy Tonight,\\u201d then took her story to Twitter.\",\n" +
            "               \"copyright\":\"Tawni Bannister for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/4dafaff7-7752-5fd1-99c5-a593bd7d237e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/opinion\\/abortion-alabama-law.html\",\n" +
            "         \"adx_keywords\":\"Abortion;State Legislatures;Law and Legislation;Roe v Wade (Supreme Court Decision);Ivey, Kay;Alabama;Georgia;Kentucky;Mississippi;Ohio\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":8,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006510271,\n" +
            "         \"asset_id\":100000006510271,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By THE EDITORIAL BOARD\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"How to Help Protect Abortion Rights in Alabama and Georgia\",\n" +
            "         \"abstract\":\"America is in an era of extreme anti-abortion laws.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 20:08:32\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"STATE LEGISLATURES\",\n" +
            "            \"LAW AND LEGISLATION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"IVEY, KAY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\",\n" +
            "            \"GEORGIA\",\n" +
            "            \"KENTUCKY\",\n" +
            "            \"MISSISSIPPI\",\n" +
            "            \"OHIO\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Protesting a ban on nearly all abortions outside of the Alabama State House in Montgomery on Tuesday.\",\n" +
            "               \"copyright\":\"Mickey Welsh\\/The Montgomery Advertiser, via Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/15alabamaWeb\\/15alabamaWeb-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/15alabamaWeb\\/15alabamaWeb-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/15alabamaWeb\\/15alabamaWeb-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/596b004c-2793-5c92-8a95-9cdf04000d84\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/us\\/abortion-laws-2019.html\",\n" +
            "         \"adx_keywords\":\"Abortion;State Legislatures;Law and Legislation;Politics and Government;Roe v Wade (Supreme Court Decision);Supreme Court (US);Alabama;Ivey, Kay;Illinois\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":9,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006511114,\n" +
            "         \"asset_id\":100000006511114,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By SABRINA TAVERNISE\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"\\u2018The Time Is Now\\u2019: States Are Rushing to Restrict Abortion, or to Protect It\",\n" +
            "         \"abstract\":\"States across the country are passing some of the most restrictive abortion laws in decades, including in Alabama, where Gov. Kay Ivey signed a bill effectively banning the procedure.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 16:34:11\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"LAW AND LEGISLATION\",\n" +
            "            \"POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"STATE LEGISLATURES\",\n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\",\n" +
            "            \"SUPREME COURT (US)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"IVEY, KAY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\",\n" +
            "            \"ILLINOIS\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Gov. Kay Ivey of Alabama signed a bill that virtually outlaws abortion in the state on Wednesday.\",\n" +
            "               \"copyright\":\"Blake Paterson\\/Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15ABORTION-HPPROMO-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15US-Briefing-PM-slide-72E3-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15US-Briefing-PM-slide-72E3-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/62555284-fece-518a-8d7c-58c0c743738a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/style\\/james-charles-makeup-artist-youtube.html\",\n" +
            "         \"adx_keywords\":\"Social Media;Fashion and Apparel;Cosmetics and Toiletries;Charles, James (Makeup Artist);Westbrook, Tati;YouTube.com\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":10,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"id\":100000006509205,\n" +
            "         \"asset_id\":100000006509205,\n" +
            "         \"nytdsection\":\"style\",\n" +
            "         \"byline\":\"By VALERIYA SAFRONOVA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"James Charles, From \\u2018CoverBoy\\u2019 to Canceled\",\n" +
            "         \"abstract\":\"The 19-year-old internet personality and makeup artist has provoked the ire of beauty YouTube.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 03:46:49\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"FASHION AND APPAREL\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"COSMETICS AND TOILETRIES\",\n" +
            "            \"YOUTUBE.COM\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"CHARLES, JAMES (MAKEUP ARTIST)\",\n" +
            "            \"WESTBROOK, TATI\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"James Charles at Gucci&rsquo;s Met Gala after-party on May 6. In the week since, he lost millions of YouTube subscribers after another influencer, Tati Westbrook, shared a critical video about him.\",\n" +
            "               \"copyright\":\"Krista Schlueter for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/14Jamescharles-met-gala-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/merlin_154504278_191f95fd-0be9-4b5b-84fa-bca67f4730a9-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/merlin_154504278_191f95fd-0be9-4b5b-84fa-bca67f4730a9-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/f0598a1b-04ca-5a5c-8aff-528f6036924d\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/upshot\\/miscarriages-politics-georgia-law.html\",\n" +
            "         \"adx_keywords\":\"Miscarriages;Abortion;Pregnancy and Childbirth;Georgia;Women and Girls;Law and Legislation\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":11,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":\"The New Health Care\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"The Upshot\",\n" +
            "         \"id\":100000006504229,\n" +
            "         \"asset_id\":100000006504229,\n" +
            "         \"nytdsection\":\"the upshot\",\n" +
            "         \"byline\":\"By AARON E. CARROLL\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Why Politics Should Be Kept Out of Miscarriages\",\n" +
            "         \"abstract\":\"The possible problems of a new Georgia law, including causing further pain.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 16:42:24\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"PREGNANCY AND CHILDBIRTH\",\n" +
            "            \"WOMEN AND GIRLS\",\n" +
            "            \"LAW AND LEGISLATION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"MISCARRIAGES\",\n" +
            "            \"ABORTION\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"GEORGIA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Georgia State troopers keeping watch over protestors in Atlanta last week after a bill was signed banning abortions as early as six weeks into pregnancy.\\u00a0\",\n" +
            "               \"copyright\":\"Alyssa Pointer\\/Atlanta Journal-Constitution, via Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/upshot\\/14up-miscarriage\\/14up-miscarriage-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/upshot\\/14up-miscarriage\\/merlin_154526004_bdc1011f-3380-4828-a424-01d4d6b948a2-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/upshot\\/14up-miscarriage\\/merlin_154526004_bdc1011f-3380-4828-a424-01d4d6b948a2-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b2883d99-4903-5164-af5d-42a04c3de88a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/03\\/14\\/us\\/politics\\/mueller-report-public.html\",\n" +
            "         \"adx_keywords\":\"United States Politics and Government;Russian Interference in 2016 US Elections and Ties to Trump Associates;Special Prosecutors (Independent Counsel);Barr, William P;Mueller, Robert S III;Trump, Donald J\",\n" +
            "         \"subsection\":\"politics\",\n" +
            "         \"share_count\":12,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006410140,\n" +
            "         \"asset_id\":100000006410140,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By NICHOLAS FANDOS\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"House Votes, 420-to-0, to Demand Public Release of Mueller Report\",\n" +
            "         \"abstract\":\"Four Republicans voted present but most joined in support as Democrats tried to pressure the attorney general to release the full findings once the inquiry concludes.\",\n" +
            "         \"published_date\":\"2019-03-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-03-22 21:32:06\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"RUSSIAN INTERFERENCE IN 2016 US ELECTIONS AND TIES TO TRUMP ASSOCIATES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"SPECIAL PROSECUTORS (INDEPENDENT COUNSEL)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BARR, WILLIAM P\",\n" +
            "            \"MUELLER, ROBERT S III\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Robert S. Mueller III, the special counsel, on Capitol Hill in 2017. The House resolution could pressure William P. Barr, the attorney general, to publicly release Mr. Mueller\\u2019s report.\",\n" +
            "               \"copyright\":\"Doug Mills\\/The New York\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/14\\/us\\/politics\\/14dc-mueller\\/14dc-mueller-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/14\\/us\\/politics\\/14dc-mueller\\/14dc-mueller-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/14\\/us\\/politics\\/14dc-mueller\\/14dc-mueller-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/a46874a5-a51c-5fc0-9aae-b7d113432a03\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/opinion\\/race-identity\\/harvard-law-harvey-weinstein.html\",\n" +
            "         \"adx_keywords\":\"Sullivan, Ronald S Jr;Harvard Law School;Harvard University;Weinstein, Harvey;Khurana, Rakesh;Colleges and Universities;Law Schools;#MeToo Movement;Legal Profession\",\n" +
            "         \"subsection\":\"opinion | race & identity\",\n" +
            "         \"share_count\":13,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006509000,\n" +
            "         \"asset_id\":100000006509000,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By RANDALL KENNEDY\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Harvard Betrays a Law Professor \\u2014 and Itself\",\n" +
            "         \"abstract\":\"Misguided students believe that defending Harvey Weinstein makes Ronald Sullivan unfit to be their dean. Apparently the university agrees.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 15:07:43\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"COLLEGES AND UNIVERSITIES\",\n" +
            "            \"#METOO MOVEMENT\",\n" +
            "            \"LEGAL PROFESSION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"HARVARD LAW SCHOOL\",\n" +
            "            \"HARVARD UNIVERSITY\",\n" +
            "            \"LAW SCHOOLS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"SULLIVAN, RONALD S JR\",\n" +
            "            \"WEINSTEIN, HARVEY\",\n" +
            "            \"KHURANA, RAKESH\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Ronald Sullivan, left, arriving at State Supreme Court in Manhattan with his client Harvey Weinstein, third from left, in January.\",\n" +
            "               \"copyright\":\"Timothy A. Clary\\/Agence France-Presse \\u2014 Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/443464b8-4c73-5152-bfca-0bfad7d14e8a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/nyregion\\/measles-vaccine-orthodox-jews.html\",\n" +
            "         \"adx_keywords\":\"Jews and Judaism;Measles;Vaccination and Immunization;Autism;Wakefield, Andrew;Rockland County (NY);Monsey (NY);Handler, Hillel;Children and Childhood;Demonstrations, Protests and Riots\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":14,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"New York\",\n" +
            "         \"id\":100000006508397,\n" +
            "         \"asset_id\":100000006508397,\n" +
            "         \"nytdsection\":\"new york\",\n" +
            "         \"byline\":\"By KIMIKO de FREYTAS-TAMURA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Despite Measles Warnings, Anti-Vaccine Rally Draws Hundreds of Ultra-Orthodox Jews\",\n" +
            "         \"abstract\":\"A \\u201cvaccine symposium\\u201d in Rockland County was denounced by health authorities and some ultra-Orthodox rabbis, who said the speakers were spreading dangerous propaganda.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 16:32:49\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"JEWS AND JUDAISM\",\n" +
            "            \"MEASLES\",\n" +
            "            \"VACCINATION AND IMMUNIZATION\",\n" +
            "            \"CHILDREN AND CHILDHOOD\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"AUTISM\",\n" +
            "            \"DEMONSTRATIONS, PROTESTS AND RIOTS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"WAKEFIELD, ANDREW\",\n" +
            "            \"HANDLER, HILLEL\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ROCKLAND COUNTY (NY)\",\n" +
            "            \"MONSEY (NY)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Hundreds were in attendance at an anti-vaccine rally in Monsey, N.Y., in Rockland County.\",\n" +
            "               \"copyright\":\"Gwynne Hogan\\/New York Public Radio\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/nyregion\\/15measles-print-promo\\/15measles-print-promo-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/nyregion\\/15measles-print-promo\\/15measles-print-promo-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/nyregion\\/15measles-print-promo\\/15measles-print-promo-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/3c575fd4-e756-5588-91ca-b1488a15831d\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/13\\/world\\/middleeast\\/us-military-plans-iran.html\",\n" +
            "         \"adx_keywords\":\"Iran;United States International Relations;United States Defense and Military Forces;United States Politics and Government;Islamic Revolutionary Guards Corps;National Security Council;Bolton, John R;Shanahan, Patrick M (1962- );Pompeo, Mike;Trump, Donald J;Nuclear Weapons;Defense Department\",\n" +
            "         \"subsection\":\"middle east\",\n" +
            "         \"share_count\":15,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"World\",\n" +
            "         \"id\":100000006507083,\n" +
            "         \"asset_id\":100000006507083,\n" +
            "         \"nytdsection\":\"world\",\n" +
            "         \"byline\":\"By ERIC SCHMITT and JULIAN E. BARNES\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"White House Reviews Military Plans Against Iran, in Echoes of Iraq War\",\n" +
            "         \"abstract\":\"The plans call for up to 120,000 American troops but not a land invasion of Iran. They were updated at the request of John R. Bolton, the national security adviser.\",\n" +
            "         \"published_date\":\"2019-05-13\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 19:33:40\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES INTERNATIONAL RELATIONS\",\n" +
            "            \"UNITED STATES DEFENSE AND MILITARY FORCES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"ISLAMIC REVOLUTIONARY GUARDS CORPS\",\n" +
            "            \"NATIONAL SECURITY COUNCIL\",\n" +
            "            \"NUCLEAR WEAPONS\",\n" +
            "            \"DEFENSE DEPARTMENT\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BOLTON, JOHN R\",\n" +
            "            \"SHANAHAN, PATRICK M (1962- )\",\n" +
            "            \"POMPEO, MIKE\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"IRAN\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"The aircraft carrier Abraham Lincoln last week in the Persian Gulf. As a precaution, the Pentagon has moved an aircraft carrier and more naval firepower to the gulf region.\",\n" +
            "               \"copyright\":\"U.S. Navy, via Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/us\\/politics\\/13dc-military1\\/13dc-military1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/us\\/politics\\/13dc-military1\\/merlin_154774206_93e8940b-0f98-4a1b-a2b4-0aeae468332c-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/us\\/politics\\/13dc-military1\\/merlin_154774206_93e8940b-0f98-4a1b-a2b4-0aeae468332c-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/ddf57db8-d0c2-510e-b02d-b5504c693b17\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/world\\/middleeast\\/trump-iran-threats.html\",\n" +
            "         \"adx_keywords\":\"Iran;Trump, Donald J;United States International Relations;United States Defense and Military Forces;United States Politics and Government;Embargoes and Sanctions;Diplomatic Service, Embassies and Consulates;State Department;United States Central Command;Bolton, John R;Pompeo, Mike\",\n" +
            "         \"subsection\":\"middle east\",\n" +
            "         \"share_count\":16,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":\"\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"World\",\n" +
            "         \"id\":100000006508984,\n" +
            "         \"asset_id\":100000006508984,\n" +
            "         \"nytdsection\":\"world\",\n" +
            "         \"byline\":\"By HELENE COOPER and EDWARD WONG\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Skeptical U.S. Allies Resist Trump\\u2019s New Claims of Threats From Iran\",\n" +
            "         \"abstract\":\"The Trump administration is laying the groundwork for major military action against Iran, but it may have a hard time rallying domestic and international support.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 16:37:57\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES INTERNATIONAL RELATIONS\",\n" +
            "            \"UNITED STATES DEFENSE AND MILITARY FORCES\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"EMBARGOES AND SANCTIONS\",\n" +
            "            \"DIPLOMATIC SERVICE, EMBASSIES AND CONSULATES\",\n" +
            "            \"STATE DEPARTMENT\",\n" +
            "            \"UNITED STATES CENTRAL COMMAND\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"TRUMP, DONALD J\",\n" +
            "            \"BOLTON, JOHN R\",\n" +
            "            \"POMPEO, MIKE\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"IRAN\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Doug Mills\\/The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2018\\/05\\/10\\/world\\/middleeast\\/bolton-video\\/bolton-video-thumbStandard-v4.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2018\\/05\\/10\\/world\\/middleeast\\/bolton-video\\/bolton-video-mediumThreeByTwo210-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2018\\/05\\/10\\/world\\/middleeast\\/bolton-video\\/bolton-video-mediumThreeByTwo440-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/2ce298f5-cca5-52c2-91bc-6423d182783a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/style\\/are-you-a-millennial.html\",\n" +
            "         \"adx_keywords\":\"Millennial Generation;Social Media;Dating and Relationships;Blockbuster Inc;Hill, Anita;Lewinsky, Monica S;Shafrir, Doree;your-feed-selfcare;Generation X\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":17,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":\"\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"id\":100000006501813,\n" +
            "         \"asset_id\":100000006501813,\n" +
            "         \"nytdsection\":\"style\",\n" +
            "         \"byline\":\"\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Are You Secretly a Millennial?\",\n" +
            "         \"abstract\":\"Xennials live in the cusp between Gen X and millennial. We can sort you with this quiz.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 16:31:13\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"MILLENNIAL GENERATION\",\n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"YOUR-FEED-SELFCARE\",\n" +
            "            \"GENERATION X\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"DATING AND RELATIONSHIPS\",\n" +
            "            \"BLOCKBUSTER INC\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"HILL, ANITA\",\n" +
            "            \"LEWINSKY, MONICA S\",\n" +
            "            \"SHAFRIR, DOREE\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":null,\n" +
            "               \"copyright\":\"MTV (Daria); Universal Pictures (Reality Bites)\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-quiz-X\\/bf77d9575bec4d8aa10396c1c3dafae7-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-quiz-X\\/bf77d9575bec4d8aa10396c1c3dafae7-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-quiz-X\\/bf77d9575bec4d8aa10396c1c3dafae7-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/440669b4-dfe9-51c2-8c28-2ca21b9d8edc\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/us\\/appalachian-trail-murder.html\",\n" +
            "         \"adx_keywords\":\"Hikes and Hiking;Murders, Attempted Murders and Homicides;Jordan, James L;Appalachian Trail;Sanchez, Ronald S Jr (d 2019)\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":18,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006508891,\n" +
            "         \"asset_id\":100000006508891,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By MICHAEL WINES\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Hikers Take to the Appalachian Trail to Escape the Real World. This Time, They Couldn\\u2019t.\",\n" +
            "         \"abstract\":\"Hikers on the trail enjoy nature and create a community. When a threatening 30-year-old joined them last month in North Carolina, word spread quickly.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 17:19:20\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"HIKES AND HIKING\",\n" +
            "            \"MURDERS, ATTEMPTED MURDERS AND HOMICIDES\"\n" +
            "         ],\n" +
            "         \"org_facet\":\"\",\n" +
            "         \"per_facet\":[  \n" +
            "            \"JORDAN, JAMES L\",\n" +
            "            \"SANCHEZ, RONALD S JR (D 2019)\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"APPALACHIAN TRAIL\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"A hiker on the Appalachian Trail just north of Ashby Gap in Virginia.\",\n" +
            "               \"copyright\":\"Samuel Corum for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15-appalachianmurder-promo\\/15-appalachianmurder-promo-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15-appalachianmurder-promo\\/15-appalachianmurder-promo-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15-appalachianmurder-promo\\/15-appalachianmurder-promo-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/7fc3133c-5e18-5d37-808a-7b0e76c34a66\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/09\\/opinion\\/sunday\\/chris-hughes-facebook-zuckerberg.html\",\n" +
            "         \"adx_keywords\":\"Social Media;Antitrust Laws and Competition Issues;Facebook Inc;Zuckerberg, Mark E;your-feed-opinionvideo\",\n" +
            "         \"subsection\":\"sunday review\",\n" +
            "         \"share_count\":19,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006497865,\n" +
            "         \"asset_id\":100000006497865,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By CHRIS HUGHES\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"It\\u2019s Time to Break Up Facebook\",\n" +
            "         \"abstract\":\"Mark Zuckerberg is a good guy. But the company I helped him build is a threat to our economy and democracy.\",\n" +
            "         \"published_date\":\"2019-05-09\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 19:25:15\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"YOUR-FEED-OPINIONVIDEO\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ANTITRUST LAWS AND COMPETITION ISSUES\",\n" +
            "            \"FACEBOOK INC\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"ZUCKERBERG, MARK E\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Jessica Chou for The New York Times (Zuckerberg); Damon Winter\\/The New York Times (Hughes)\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b5f57184-e287-5ec9-bb78-f0d50dc36a4c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/us\\/facial-recognition-ban-san-francisco.html\",\n" +
            "         \"adx_keywords\":\"San Francisco (Calif);Computer Vision;Face;Surveillance of Citizens by Government;Privacy;Politics and Government;Police\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"share_count\":20,\n" +
            "         \"count_type\":\"SHARED-FACEBOOK\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006509114,\n" +
            "         \"asset_id\":100000006509114,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By KATE CONGER, RICHARD FAUSSET and SERGE F. KOVALESKI\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"San Francisco Bans Facial Recognition Technology\",\n" +
            "         \"abstract\":\"It is the first ban by a major city on the use of facial recognition technology by the police and all other municipal agencies.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 13:17:43\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"COMPUTER VISION\",\n" +
            "            \"FACE\",\n" +
            "            \"SURVEILLANCE OF CITIZENS BY GOVERNMENT\",\n" +
            "            \"POLICE\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"PRIVACY\",\n" +
            "            \"POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"SAN FRANCISCO (CALIF)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Attendees interacting with a facial recognition demonstration at this year\\u2019s CES in Las Vegas.\",\n" +
            "               \"copyright\":\"Joe Buglewicz for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b6c5a8d0-5c8f-573a-ad47-8829c6c5fc38\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";




    //IIIIIIIIIIIIIIIIIIIIIIIIIIIII














    String name, salary;
    TextView employeeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mostshareartciles);

        drawer = findViewById(R.id.drawer_layout);
        aw();



/*
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Home Page");
        toolbar.inflateMenu(R.menu.menu_context);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.citm4)
                {

                    String url = "https://www.nytimes.com/";

                    // Parse the URI and create the intent.
                    Uri webpage = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

                    // Find an activity to hand the intent and start that activity.
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        Log.d("ImplicitIntents", "Can't handle this intent!");
                    }

                }


                return false;
            }
        });
*/
        // get the reference of TextView's
        employeeName = (TextView) findViewById(R.id.name2);
        String data ="";
        String dataParsed = "";
        String singleParsed ="";
        try {


            // get JSONObject from JSON file
            // JSONObject obj = new JSONObject(Json_string2);
            // fetch JSONObject named employee
            // JSONObject employee = obj.getJSONObject("results");
            // get employee name and salary
            //  name = employee.getString("list_name");
            // set employee name and salary in TextView's
            //  employeeName.setText("Name: " + name);



            JSONObject jsonObj = new JSONObject(JASON);
            String p = "";String p2 = " ";
            JSONArray c = jsonObj.getJSONArray("results");

            for (int i = 1 ; i < c.length(); i++) {
                JSONObject obj = c.getJSONObject(i);
                String A = obj.getString("url");
                String B = obj.getString("source");
                String C = obj.getString("updated");



                p +=  " \nUrl:\n" + A + "\n "  + "Source:\n" +  B + "\n" + "Updated:\n " + C + "\n";

            }

            sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


            mEditor = sp.edit();
            mEditor.putString("link", p);
            mEditor.commit();

          //  employeeName.setText(p);  imp


            //   employeeName.setText(p);
            // employeeSalary.setText(p2);



            //JSONArray JA = new JSONArray(Json_string2);
            //   for(int i =0 ;i <JA.length(); i++){
            //      JSONObject JO = (JSONObject) JA.get(i);
            //       singleParsed =  "Name:" + JO.get("list_name");


            //    dataParsed = dataParsed + singleParsed +"\n" ;


            //   }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        // employeeName.setText("H " + dataParsed);

    }

    private void aw() {

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();

        MenuItem gallery = menu.findItem(R.id.nav_gallery);
        MenuItem slideshow = menu.findItem(R.id.nav_slideshow);
        MenuItem tools = menu.findItem(R.id.nav_tools);
        MenuItem books = menu.findItem(R.id.bestbooks);


        // set new title to the MenuItem
        gallery.setTitle("Most Emailed Articles2");
        slideshow.setTitle("Most Shared Articles");
        tools.setTitle("Most Viewed Articles");
        books.setTitle("BestBooks");
       MenuItem search = menu.findItem(R.id.share);

       search.setTitle("search books");


        navigationView.setNavigationItemSelectedListener(this);


    }


    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            // setTitle("#");
            //  first p = new first();
            // FragmentManager frag = getSupportFragmentManager();
            //   frag.beginTransaction().replace(R.id.fragment,p).commit();
            Intent intentMain = new Intent(MostSharedArticles.this,
                    MainActivity.class);
            MostSharedArticles.this.startActivity(intentMain);


        } else if (id == R.id.nav_gallery) {

            Intent intentMain = new Intent(MostSharedArticles.this,
                    MostSharedArticles.class);
            MostSharedArticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_slideshow) {
            Intent intentMain = new Intent(MostSharedArticles.this,
                    mostemailedarticles.class);
            MostSharedArticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_tools) {
            Intent intentMain = new Intent(MostSharedArticles.this,
                    mostviewedarticles.class);
            MostSharedArticles.this.startActivity(intentMain);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
     else if (id == R.id.bestbooks) {
        Intent intentMain = new Intent(MostSharedArticles.this,
                bestBooks.class);
            MostSharedArticles.this.startActivity(intentMain);


    }
        else if (id == R.id.share) {
            Intent intentMain = new Intent(MostSharedArticles.this,
                    search.class);
            MostSharedArticles.this.startActivity(intentMain);


        }



        return true;

    }





    @Override
    protected void onStart() {
        super.onStart();

        StringBuilder str = new StringBuilder();
        if (sp.contains(KEY_NAME)) {
            employeeName.setText("fg" + sp.getString("link", ""));
        }

    }



    protected void onResume() {
        super.onResume();

        StringBuilder str = new StringBuilder();
        if (sp.contains(KEY_NAME)) {
            employeeName.setText("og" + sp.getString("link", ""));
        }

    }


}
