package com.example.afinal;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class mostemailedarticles extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private Toolbar toolbar;

    String JASON = "{  \n" +
            "   \"status\":\"OK\",\n" +
            "   \"copyright\":\"Copyright (c) 2019 The New York Times Company.  All Rights Reserved.\",\n" +
            "   \"num_results\":713,\n" +
            "   \"results\":[  \n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/09\\/opinion\\/sunday\\/chris-hughes-facebook-zuckerberg.html\",\n" +
            "         \"adx_keywords\":\"Social Media;Antitrust Laws and Competition Issues;Facebook Inc;Zuckerberg, Mark E;your-feed-opinionvideo\",\n" +
            "         \"subsection\":\"sunday review\",\n" +
            "         \"email_count\":1,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006497865,\n" +
            "         \"asset_id\":100000006497865,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By CHRIS HUGHES\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"It\\u2019s Time to Break Up Facebook\",\n" +
            "         \"abstract\":\"Mark Zuckerberg is a good guy. But the company I helped him build is a threat to our economy and democracy.\",\n" +
            "         \"published_date\":\"2019-05-09\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 19:25:15\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"YOUR-FEED-OPINIONVIDEO\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ANTITRUST LAWS AND COMPETITION ISSUES\",\n" +
            "            \"FACEBOOK INC\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"ZUCKERBERG, MARK E\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Jessica Chou for The New York Times (Zuckerberg); Damon Winter\\/The New York Times (Hughes)\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/opinion\\/sunday\\/09Hughes\\/09Hughes-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b5f57184-e287-5ec9-bb78-f0d50dc36a4c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/opinion\\/trump-willmar-minnesota.html\",\n" +
            "         \"adx_keywords\":\"Willmar (Minn);Immigration and Emigration;Somali-Americans;Hispanic-Americans;Calvin, Marv;Trump, Donald J;Labor and Jobs;Education (K-12)\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":2,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006507843,\n" +
            "         \"asset_id\":100000006507843,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By THOMAS L. FRIEDMAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"President Trump, Come to Willmar\",\n" +
            "         \"abstract\":\"This Minnesota town is a modern, successful American melting pot.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 20:59:54\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOMALI-AMERICANS\",\n" +
            "            \"EDUCATION (K-12)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"IMMIGRATION AND EMIGRATION\",\n" +
            "            \"HISPANIC-AMERICANS\",\n" +
            "            \"LABOR AND JOBS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"CALVIN, MARV\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"WILLMAR (MINN)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Faiza Dalmar Awil crossing the street between her restaurant, Somali Star, and her shop in Willmar, Minn.\",\n" +
            "               \"copyright\":\"Jenn Ackerman for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/14friedman7\\/14friedman7-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/7fd2805e-7e30-5ec2-9a76-2560d4d4ed3f\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/07\\/us\\/politics\\/donald-trump-taxes.html\",\n" +
            "         \"adx_keywords\":\"Trump, Donald J;Trump, Fred C;Inheritance and Estate Taxes;Income Tax;Corporate Taxes;Trump Tax Returns;Trump Organization;United States Politics and Government;Presidents and Presidency (US)\",\n" +
            "         \"subsection\":\"politics\",\n" +
            "         \"email_count\":3,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"id\":100000006417857,\n" +
            "         \"asset_id\":100000006417857,\n" +
            "         \"nytdsection\":\"u.s.\",\n" +
            "         \"byline\":\"By RUSS BUETTNER and SUSANNE CRAIG\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Decade in the Red: Trump Tax Figures Show Over $1 Billion in Business Losses\",\n" +
            "         \"abstract\":\"Newly obtained tax information reveals that from 1985 to 1994, Donald J. Trump\\u2019s businesses were in far bleaker condition than was previously known.\",\n" +
            "         \"published_date\":\"2019-05-07\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-08 21:00:17\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"INHERITANCE AND ESTATE TAXES\",\n" +
            "            \"INCOME TAX\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"PRESIDENTS AND PRESIDENCY (US)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"CORPORATE TAXES\",\n" +
            "            \"TRUMP TAX RETURNS\",\n" +
            "            \"TRUMP ORGANIZATION\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"TRUMP, DONALD J\",\n" +
            "            \"TRUMP, FRED C\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Donald J. Trump in 1986, during a tumultuous period of his career marked by acquisition and collapse.\",\n" +
            "               \"copyright\":\"Ted Thai\\/The LIFE Picture Collection, via Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/20\\/obituaries\\/archives\\/00trump-ice\\/00trump-ice-thumbStandard-v4.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/20\\/obituaries\\/archives\\/00trump-ice\\/00trump-ice-mediumThreeByTwo210-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/03\\/20\\/obituaries\\/archives\\/00trump-ice\\/00trump-ice-mediumThreeByTwo440-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/2279d709-d225-5524-8ba1-e0d0f67e222c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/10\\/health\\/assisted-living-costs-elderly.html\",\n" +
            "         \"adx_keywords\":\"Elderly;Income;Retirement Communities and Assisted Living;Long-Term Care Insurance;Medicaid;Medicare;Health Affairs (Journal);National Opinion Research Center\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":4,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\" The New Old Age\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Health\",\n" +
            "         \"id\":100000006500397,\n" +
            "         \"asset_id\":100000006500397,\n" +
            "         \"nytdsection\":\"health\",\n" +
            "         \"byline\":\"By PAULA SPAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Many Americans Will Need Long-Term Care. Most Won\\u2019t be Able to Afford It.\",\n" +
            "         \"abstract\":\"A decade from now, most middle-income seniors will not be able to pay the rising costs of independent or assisted living.\",\n" +
            "         \"published_date\":\"2019-05-10\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 04:42:55\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ELDERLY\",\n" +
            "            \"INCOME\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"RETIREMENT COMMUNITIES AND ASSISTED LIVING\",\n" +
            "            \"LONG-TERM CARE INSURANCE\",\n" +
            "            \"MEDICAID\",\n" +
            "            \"MEDICARE\",\n" +
            "            \"HEALTH AFFAIRS (JOURNAL)\",\n" +
            "            \"NATIONAL OPINION RESEARCH CENTER\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Gretchen Harris, with her dog Alfie, at home in Norman, Okla. Paying for long-term care &ldquo;weighs on my mind some,&rdquo; she said.\",\n" +
            "               \"copyright\":\"Brett Deering for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/10SCI-SPAN\\/10SCI-SPAN-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/10SCI-SPAN\\/10SCI-SPAN-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/10SCI-SPAN\\/10SCI-SPAN-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/f9f4f6e8-e9bd-57b2-ac87-e0b5806647de\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/08\\/magazine\\/best-green-salad-recipe.html\",\n" +
            "         \"adx_keywords\":\"Cooking and Cookbooks;Salads;Recipes;Salad Dressings\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":5,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"Eat\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"id\":100000006489097,\n" +
            "         \"asset_id\":100000006489097,\n" +
            "         \"nytdsection\":\"magazine\",\n" +
            "         \"byline\":\"By SAMIN NOSRAT\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"The Best Green Salad in the World\",\n" +
            "         \"abstract\":\"There\\u2019s a secret, but it\\u2019s available to all.\",\n" +
            "         \"published_date\":\"2019-05-08\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-13 13:09:00\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"COOKING AND COOKBOOKS\",\n" +
            "            \"SALADS\",\n" +
            "            \"SALAD DRESSINGS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"RECIPES\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Via Carota\\u2019s <em>insalata verde<\\/em>.\",\n" +
            "               \"copyright\":\"Bobby Doherty for The New York Times. Food Stylist: Maggie Ruggiero. Prop stylist: Rebecca Bartoshesky. \",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12mag-eat-1\\/12mag-eat-1-thumbStandard-v2.png\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12mag-eat-1\\/6091444e4e61403e92e4e9c0f5303477-mediumThreeByTwo210.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12mag-eat-1\\/6091444e4e61403e92e4e9c0f5303477-mediumThreeByTwo440.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/8e09f10e-2dc9-5048-ae3f-5b0b053814a8\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/12\\/science\\/5g-phone-safety-health-russia.html\",\n" +
            "         \"adx_keywords\":\"5G (Wireless Communications);RT America (TV Network);RT (TV Network);Russia;Radiation;Rumors and Misinformation;Cellular Telephones;Propaganda;Cancer;Brain Cancer;Wireless Communications;News and News Media;United States International Relations;Television;Radio Spectrum;United States Politics and Government;Social Media;X-Rays;Putin, Vladimir V;your-feed-science;Carpenter, David O\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":6,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Science\",\n" +
            "         \"id\":100000006454376,\n" +
            "         \"asset_id\":100000006454376,\n" +
            "         \"nytdsection\":\"science\",\n" +
            "         \"byline\":\"By WILLIAM J. BROAD\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Your 5G Phone Won\\u2019t Hurt You. But Russia Wants You to Think Otherwise.\",\n" +
            "         \"abstract\":\"RT America, a network known for sowing disinformation, has a new alarm: the coming \\u20185G Apocalypse.\\u2019\",\n" +
            "         \"published_date\":\"2019-05-12\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 15:14:02\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"5G (WIRELESS COMMUNICATIONS)\",\n" +
            "            \"UNITED STATES INTERNATIONAL RELATIONS\",\n" +
            "            \"YOUR-FEED-SCIENCE\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"RT AMERICA (TV NETWORK)\",\n" +
            "            \"RT (TV NETWORK)\",\n" +
            "            \"RADIATION\",\n" +
            "            \"RUMORS AND MISINFORMATION\",\n" +
            "            \"CELLULAR TELEPHONES\",\n" +
            "            \"PROPAGANDA\",\n" +
            "            \"CANCER\",\n" +
            "            \"BRAIN CANCER\",\n" +
            "            \"WIRELESS COMMUNICATIONS\",\n" +
            "            \"NEWS AND NEWS MEDIA\",\n" +
            "            \"TELEVISION\",\n" +
            "            \"RADIO SPECTRUM\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"X-RAYS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"PUTIN, VLADIMIR V\",\n" +
            "            \"CARPENTER, DAVID O\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"RUSSIA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"A Russia Today anchor in Moscow. The network\\u2019s American version, RT America, has been exaggerating the health hazards posed by 5G networks.\",\n" +
            "               \"copyright\":\"Yuri Kadobnov\\/Agence France-Presse \\u2014 Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/04\\/11\\/science\\/005GPROPAGANDA1-promo\\/005GPROPAGANDA1-promo-thumbStandard-v3.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/04\\/11\\/science\\/005GPROPAGANDA1-promo\\/005GPROPAGANDA1-promo-mediumThreeByTwo210-v5.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/04\\/11\\/science\\/005GPROPAGANDA1-promo\\/005GPROPAGANDA1-promo-mediumThreeByTwo440-v5.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/26be5195-5695-5daa-aae1-5d8e0b9b969e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/11\\/opinion\\/sunday\\/generic-drugs-safety.html\",\n" +
            "         \"adx_keywords\":\"Drugs (Pharmaceuticals);Factories and Manufacturing;Generic Brands and Products;Regulation and Deregulation of Industry;Wockhardt Ltd;India\",\n" +
            "         \"subsection\":\"sunday review\",\n" +
            "         \"email_count\":7,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006499704,\n" +
            "         \"asset_id\":100000006499704,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By KATHERINE EBAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Americans Need Generic Drugs. But Can They Trust Them?\",\n" +
            "         \"abstract\":\"The fake quality-control data, bird infestations and toxic impurities at the overseas plants that could be making your medication.\",\n" +
            "         \"published_date\":\"2019-05-11\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-13 22:11:36\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"DRUGS (PHARMACEUTICALS)\",\n" +
            "            \"FACTORIES AND MANUFACTURING\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"GENERIC BRANDS AND PRODUCTS\",\n" +
            "            \"REGULATION AND DEREGULATION OF INDUSTRY\",\n" +
            "            \"WOCKHARDT LTD\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"INDIA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Delcan & Company\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12eban\\/12eban-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12eban\\/12eban-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12eban\\/12eban-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/a886b4b7-5b0d-548b-8f56-81f5ea9cebaf\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/07\\/opinion\\/trump-2020.html\",\n" +
            "         \"adx_keywords\":\"Presidential Election of 2020;Geller, Uri;Republican Party;Democratic Party;Trump, Donald J;United States Politics and Government;Third-Party Politics (US)\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":8,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006495674,\n" +
            "         \"asset_id\":100000006495674,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By THOMAS L. FRIEDMAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"How to Defeat Trump\",\n" +
            "         \"abstract\":\"For starters, we need a patriotic Republican on the right to run as a third-party candidate.\",\n" +
            "         \"published_date\":\"2019-05-07\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-10 14:48:26\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"PRESIDENTIAL ELECTION OF 2020\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\",\n" +
            "            \"THIRD-PARTY POLITICS (US)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"REPUBLICAN PARTY\",\n" +
            "            \"DEMOCRATIC PARTY\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"GELLER, URI\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"President Trump at the White House on Tuesday.\",\n" +
            "               \"copyright\":\"Doug Mills\\/The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/07\\/opinion\\/07friedmanWeb\\/07friedmanWeb-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/07\\/opinion\\/07friedmanWeb\\/07friedmanWeb-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/07\\/opinion\\/07friedmanWeb\\/07friedmanWeb-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/c8a51a87-2d03-518e-a128-3c5545a2fc5d\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/08\\/magazine\\/cousin-kill-me-male-violence.html\",\n" +
            "         \"adx_keywords\":\"Men and Boys;Domestic Violence;Assaults\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":9,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"Feature\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"id\":100000006492609,\n" +
            "         \"asset_id\":100000006492609,\n" +
            "         \"nytdsection\":\"magazine\",\n" +
            "         \"byline\":\"By WIL S. HYLTON\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"My Cousin Was My Hero. Until the Day He Tried to Kill Me.\",\n" +
            "         \"abstract\":\"For years, I was drawn to his strength, his bravado, his violence. But then he forced me to come to terms with how that idea of masculinity poisoned his life \\u2014 and mine.\",\n" +
            "         \"published_date\":\"2019-05-08\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 14:23:57\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"DOMESTIC VIOLENCE\",\n" +
            "            \"ASSAULTS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"MEN AND BOYS\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Photo illustration by Mike McQuade\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12Mag-Assault-image1\\/12Mag-Assault-image1-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12Mag-Assault-image1\\/12Mag-Assault-image1-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/magazine\\/12Mag-Assault-image1\\/12Mag-Assault-image1-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/e3df9b2c-8a6c-5aa3-b4a0-4869e7b9f1aa\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/10\\/opinion\\/trump-california.html\",\n" +
            "         \"adx_keywords\":\"California;Oregon;Washington (State);Western States (US);Trump, Donald J;United States Politics and Government\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":10,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006502361,\n" +
            "         \"asset_id\":100000006502361,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By TIMOTHY EGAN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Revenge of the Coastal Elites\",\n" +
            "         \"abstract\":\"How California, Oregon and Washington are winning the fight against Trump\\u2019s hateful policies\",\n" +
            "         \"published_date\":\"2019-05-10\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-11 19:17:05\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":\"\",\n" +
            "         \"per_facet\":[  \n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"CALIFORNIA\",\n" +
            "            \"OREGON\",\n" +
            "            \"WASHINGTON (STATE)\",\n" +
            "            \"WESTERN STATES (US)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"The unveiling of President Barack Obama Boulevard in Los Angeles on Saturday.\",\n" +
            "               \"copyright\":\"Damian Dovarganes\\/Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/opinion\\/10eganWeb\\/10eganWeb-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/opinion\\/10eganWeb\\/10eganWeb-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/opinion\\/10eganWeb\\/10eganWeb-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/0ba505f8-cca3-5aec-9233-c859bce902ca\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/09\\/health\\/hospitals-prices-medicare.html\",\n" +
            "         \"adx_keywords\":\"Hospitals;Medicare;Prices (Fares, Fees and Rates);Health Insurance and Managed Care;States (US);Indiana;Colorado;Anthem Inc;Johnson, Robert Wood, Foundation;Rand Corp;your-feed-healthcare\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":11,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Health\",\n" +
            "         \"id\":100000006500115,\n" +
            "         \"asset_id\":100000006500115,\n" +
            "         \"nytdsection\":\"health\",\n" +
            "         \"byline\":\"By REED ABELSON\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Many Hospitals Charge Double or Even Triple What Medicare Would Pay\",\n" +
            "         \"abstract\":\"A study of 25 states provides a rare glimpse of the stark disparities between what private insurers and the federal government paid for inpatient and outpatient care.\",\n" +
            "         \"published_date\":\"2019-05-09\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-10 14:42:15\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"HOSPITALS\",\n" +
            "            \"MEDICARE\",\n" +
            "            \"PRICES (FARES, FEES AND RATES)\",\n" +
            "            \"HEALTH INSURANCE AND MANAGED CARE\",\n" +
            "            \"YOUR-FEED-HEALTHCARE\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"STATES (US)\",\n" +
            "            \"ANTHEM INC\",\n" +
            "            \"JOHNSON, ROBERT WOOD, FOUNDATION\",\n" +
            "            \"RAND CORP\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"INDIANA\",\n" +
            "            \"COLORADO\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"The Parkview Health System in Indiana had among the highest overall hospital prices compared to Medicare rates, according to a study of 25 states&rsquo; hospital pricing.\",\n" +
            "               \"copyright\":\"Tom Strattman For The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/science\\/10HOSPITALS\\/09HOSPITALS-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/science\\/10HOSPITALS\\/09HOSPITALS-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/10\\/science\\/10HOSPITALS\\/09HOSPITALS-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/3a42b526-cc59-5433-ba8d-b5250343033c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/13\\/opinion\\/fishing-species-extinction-yellowstone.html\",\n" +
            "         \"adx_keywords\":\"Endangered and Extinct Species;Fish and Other Marine Life;Yellowstone National Park;Fishing, Sport;Trout\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":12,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006492295,\n" +
            "         \"asset_id\":100000006492295,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By RICHARD CONNIFF\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"A Chain of Species Destruction at Yellowstone\",\n" +
            "         \"abstract\":\"The havoc caused by stocking the park\\u2019s lake for sport fishing ravaged ospreys, pelicans, bald eagles, grizzly bears and the lake\\u2019s own native cutthroat trout.\",\n" +
            "         \"published_date\":\"2019-05-13\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 20:13:50\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ENDANGERED AND EXTINCT SPECIES\",\n" +
            "            \"FISH AND OTHER MARINE LIFE\",\n" +
            "            \"TROUT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"FISHING, SPORT\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"YELLOWSTONE NATIONAL PARK\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Leonardo Santamaria\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/13conniff\\/13conniff-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/13conniff\\/13conniff-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/13conniff\\/13conniff-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/9a81e61f-25ed-5941-9673-c00340235e4b\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/12\\/well\\/live\\/a-little-known-skin-disease-that-can-disrupt-peoples-sex-lives.html\",\n" +
            "         \"adx_keywords\":\"Skin;Lichens;Women and Girls;Tissue (Human);Itching;Vagina;Sex;Menopause\",\n" +
            "         \"subsection\":\"live\",\n" +
            "         \"email_count\":13,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"Personal Health\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Well\",\n" +
            "         \"id\":100000006466314,\n" +
            "         \"asset_id\":100000006466314,\n" +
            "         \"nytdsection\":\"well\",\n" +
            "         \"byline\":\"By JANE E. BRODY\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"A Little-Known Skin Disease That Can Disrupt People\\u2019s Sex Lives\",\n" +
            "         \"abstract\":\"Patients deal with pain and itching and often encounter medical ignorance and mistreatment until affected tissues become irreparably scarred.\",\n" +
            "         \"published_date\":\"2019-05-12\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 04:42:39\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"SKIN\",\n" +
            "            \"LICHENS\",\n" +
            "            \"WOMEN AND GIRLS\",\n" +
            "            \"TISSUE (HUMAN)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ITCHING\",\n" +
            "            \"VAGINA\",\n" +
            "            \"SEX\",\n" +
            "            \"MENOPAUSE\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":null,\n" +
            "               \"copyright\":\"Gracia Lam\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/14BRODYLICHEN\\/merlin_154617417_741a15d4-57a0-4be6-afff-c1881d0af4fe-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/14BRODYLICHEN\\/merlin_154617417_741a15d4-57a0-4be6-afff-c1881d0af4fe-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/science\\/14BRODYLICHEN\\/merlin_154617417_741a15d4-57a0-4be6-afff-c1881d0af4fe-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/50fa266f-7fec-5789-b795-668cf6193567\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/11\\/opinion\\/sunday\\/math-teaching-football.html\",\n" +
            "         \"adx_keywords\":\"Mathematics;Football;Education (K-12)\",\n" +
            "         \"subsection\":\"sunday review\",\n" +
            "         \"email_count\":14,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006499611,\n" +
            "         \"asset_id\":100000006499611,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By JOHN URSCHEL\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Math Teachers Should Be More Like Football Coaches\",\n" +
            "         \"abstract\":\"That style of motivation could help in the classroom, too.\",\n" +
            "         \"published_date\":\"2019-05-11\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-12 23:25:24\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"MATHEMATICS\",\n" +
            "            \"EDUCATION (K-12)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"FOOTBALL\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Richie Pope\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12urschel\\/merlin_154640388_5b3df20f-1471-4761-8cbc-64183b68689c-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12urschel\\/12urschel-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/12\\/opinion\\/sunday\\/12urschel\\/12urschel-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b0129b5f-60d4-51fe-b845-eca278125a5c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/magazine\\/cbd-cannabis-cure.html\",\n" +
            "         \"adx_keywords\":\"2019 Health Issue;Cannabis Foods and Products;Seizures (Medical);Medical Marijuana;Marijuana\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":15,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"id\":100000006504038,\n" +
            "         \"asset_id\":100000006504038,\n" +
            "         \"nytdsection\":\"magazine\",\n" +
            "         \"byline\":\"By MOISES VELASQUEZ-MANOFF\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Can CBD Really Do All That?\",\n" +
            "         \"abstract\":\"How one molecule from the cannabis plant came to be seen as a therapeutic cure-all.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-16 18:06:10\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"CANNABIS FOODS AND PRODUCTS\",\n" +
            "            \"MARIJUANA\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"SEIZURES (MEDICAL)\",\n" +
            "            \"MEDICAL MARIJUANA\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Various products that have been advertised as containing CBD.\",\n" +
            "               \"copyright\":\"Jamie Chung for The New York Times. Prop styling by Anna Surbatovich.\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/4910bb4b-d110-5dc7-bb7c-712df5be2de3\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/opinion\\/dont-visit-your-doctor-in-the-afternoon.html\",\n" +
            "         \"adx_keywords\":\"Doctors;Fatigue\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":16,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006507138,\n" +
            "         \"asset_id\":100000006507138,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By JEFFREY A. LINDER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Don\\u2019t Visit Your Doctor in the Afternoon\",\n" +
            "         \"abstract\":\"Everyone suffers decision fatigue, even physicians.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 19:16:51\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"DOCTORS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"FATIGUE\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Strannik Fox\\/iStock, via Getty Images Plus\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/14Linder\\/14Linder-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/14Linder\\/14Linder-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/opinion\\/14Linder\\/14Linder-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/feed3a95-4343-5039-bdc8-98db6f348a52\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/09\\/your-money\\/college-application-essays-money.html\",\n" +
            "         \"adx_keywords\":\"Personal Finances;Colleges and Universities;Admissions Standards;Tuition\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":17,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":\"Your Money\",\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Your Money\",\n" +
            "         \"id\":100000006495404,\n" +
            "         \"asset_id\":100000006495404,\n" +
            "         \"nytdsection\":\"your money\",\n" +
            "         \"byline\":\"By RON LIEBER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Trash, the Library and a Worn, Brown Table: The 2019 College Essays on Money\",\n" +
            "         \"abstract\":\"Each year, we ask high school seniors to submit college application essays they\\u2019ve written about work, money, social class and related topics. Here are five that moved us.\",\n" +
            "         \"published_date\":\"2019-05-09\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-11 03:52:03\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ADMISSIONS STANDARDS\",\n" +
            "            \"TUITION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"PERSONAL FINANCES\",\n" +
            "            \"COLLEGES AND UNIVERSITIES\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Eiko Ojala\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/business\\/09money-illo\\/09money-illo-thumbStandard-v3.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/business\\/09money-illo\\/09money-illo-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/11\\/business\\/09money-illo\\/09money-illo-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/7af4647c-4d3a-51c0-99ac-7c6daaf2a681\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/09\\/dining\\/drinks\\/aperol-spritz.html\",\n" +
            "         \"adx_keywords\":\"Cocktails and Mixed Drinks;Alcoholic Beverages;Prosecco (Wine);Italy\",\n" +
            "         \"subsection\":\"wine, beer & cocktails\",\n" +
            "         \"email_count\":18,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Food\",\n" +
            "         \"id\":100000006488268,\n" +
            "         \"asset_id\":100000006488268,\n" +
            "         \"nytdsection\":\"food\",\n" +
            "         \"byline\":\"By REBEKAH PEPPLER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"The Aperol Spritz Is Not a Good Drink\",\n" +
            "         \"abstract\":\"The popular, Instagram-friendly aperitif drinks like a Capri Sun after soccer practice on a hot day. Not in a good way.\",\n" +
            "         \"published_date\":\"2019-05-09\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-15 04:48:21\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"COCKTAILS AND MIXED DRINKS\",\n" +
            "            \"ALCOHOLIC BEVERAGES\",\n" +
            "            \"PROSECCO (WINE)\"\n" +
            "         ],\n" +
            "         \"org_facet\":\"\",\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ITALY\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"You\\u2019re better than this.\",\n" +
            "               \"copyright\":\"Karsten Moran for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/dining\\/09spritz1\\/09spritz1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/dining\\/09spritz1\\/merlin_141351063_e6829ad6-f61c-435a-b6c3-87526a0613ef-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/dining\\/09spritz1\\/merlin_141351063_e6829ad6-f61c-435a-b6c3-87526a0613ef-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/fcb9ccaf-884e-551a-a5f3-a9ae45b525f1\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/06\\/smarter-living\\/why-you-need-a-network-of-low-stakes-casual-friendships.html\",\n" +
            "         \"adx_keywords\":\"Friendship\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":19,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Smarter Living\",\n" +
            "         \"id\":100000006442539,\n" +
            "         \"asset_id\":100000006442539,\n" +
            "         \"nytdsection\":\"smarter living\",\n" +
            "         \"byline\":\"By ALLIE VOLPE\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Why You Need a Network of Low-Stakes, Casual Friendships\",\n" +
            "         \"abstract\":\"Weak ties can offer strong rewards.\",\n" +
            "         \"published_date\":\"2019-05-06\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 12:22:31\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"FRIENDSHIP\"\n" +
            "         ],\n" +
            "         \"org_facet\":\"\",\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":null,\n" +
            "               \"copyright\":\"Edmon de Haro\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/smarter-living\\/13sl-friendships\\/03d85b0af0a64ca7b08bf8026483e004-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/smarter-living\\/13sl-friendships\\/03d85b0af0a64ca7b08bf8026483e004-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/13\\/smarter-living\\/13sl-friendships\\/03d85b0af0a64ca7b08bf8026483e004-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/2f142662-9059-58df-810f-5a65516bcfe8\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/13\\/opinion\\/united-nations-extinction.html\",\n" +
            "         \"adx_keywords\":\"Endangered and Extinct Species;Biodiversity;Turtles and Tortoises;Conservation of Resources;United Nations\",\n" +
            "         \"subsection\":\"\",\n" +
            "         \"email_count\":20,\n" +
            "         \"count_type\":\"EMAILED\",\n" +
            "         \"column\":null,\n" +
            "         \"eta_id\":0,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"id\":100000006502010,\n" +
            "         \"asset_id\":100000006502010,\n" +
            "         \"nytdsection\":\"opinion\",\n" +
            "         \"byline\":\"By MARGARET RENKL\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Surviving Despair in the Great Extinction\",\n" +
            "         \"abstract\":\"One million species of plants and animals are heading toward annihilation, and it\\u2019s our fault. How can we possibly live with that truth?\",\n" +
            "         \"published_date\":\"2019-05-13\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"updated\":\"2019-05-14 02:11:32\",\n" +
            "         \"des_facet\":[  \n" +
            "            \"ENDANGERED AND EXTINCT SPECIES\",\n" +
            "            \"CONSERVATION OF RESOURCES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"BIODIVERSITY\",\n" +
            "            \"TURTLES AND TORTOISES\",\n" +
            "            \"UNITED NATIONS\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Animal Press\\/Barcroft Media, via Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/opinion\\/13renklWeb1\\/13renklWeb1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/opinion\\/13renklWeb1\\/13renklWeb1-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/opinion\\/13renklWeb1\\/13renklWeb1-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/cd1692c0-bce2-56fa-89ef-590f8ad8f609\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";


    //IIIIIIIIIIIIIIIIIIIIIIIIIIIII














    String name, salary;
    TextView employeeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mostemailedarticles);
        drawer = findViewById(R.id.drawer_layout);
        aw();
        /*
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Home Page");
        toolbar.inflateMenu(R.menu.menu_context);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.citm4)
                {

                    String url = "https://www.nytimes.com/";

                    // Parse the URI and create the intent.
                    Uri webpage = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

                    // Find an activity to hand the intent and start that activity.
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        Log.d("ImplicitIntents", "Can't handle this intent!");
                    }

                }


                return false;
            }
        });
        */
      //  toolbar.inflateMenu(R.menu.menu_context);
        // get the reference of TextView's
        employeeName = (TextView) findViewById(R.id.name2);
        String data ="";
        String dataParsed = "";
        String singleParsed ="";
        try {


            // get JSONObject from JSON file
            // JSONObject obj = new JSONObject(Json_string2);
            // fetch JSONObject named employee
            // JSONObject employee = obj.getJSONObject("results");
            // get employee name and salary
            //  name = employee.getString("list_name");
            // set employee name and salary in TextView's
            //  employeeName.setText("Name: " + name);



            JSONObject jsonObj = new JSONObject(JASON);
            String p = "";String p2 = " ";
            JSONArray c = jsonObj.getJSONArray("results");

            for (int i = 1 ; i < c.length(); i++) {
                JSONObject obj = c.getJSONObject(i);
                String A = obj.getString("url");
                String B = obj.getString("source");
                String C = obj.getString("updated");



                p +=  " \nUrl:\n" + A + "\n "  + "Source:\n" +  B + "\n" + "Updated:\n " + C + "\n";

            }
            employeeName.setText(p);


            //   employeeName.setText(p);
            // employeeSalary.setText(p2);



            //JSONArray JA = new JSONArray(Json_string2);
            //   for(int i =0 ;i <JA.length(); i++){
            //      JSONObject JO = (JSONObject) JA.get(i);
            //       singleParsed =  "Name:" + JO.get("list_name");


            //    dataParsed = dataParsed + singleParsed +"\n" ;


            //   }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        // employeeName.setText("H " + dataParsed);

    }














    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            // setTitle("#");
            //  first p = new first();
            // FragmentManager frag = getSupportFragmentManager();
            //   frag.beginTransaction().replace(R.id.fragment,p).commit();
            Intent intentMain = new Intent(mostemailedarticles.this,
                    MainActivity.class);
            mostemailedarticles.this.startActivity(intentMain);


        } else if (id == R.id.nav_gallery) {

            Intent intentMain = new Intent(mostemailedarticles.this,
                    MostSharedArticles.class);
            mostemailedarticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_slideshow) {
            Intent intentMain = new Intent(mostemailedarticles.this,
                    mostemailedarticles.class);
            mostemailedarticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_tools) {
            Intent intentMain = new Intent(mostemailedarticles.this,
                    mostviewedarticles.class);
            mostemailedarticles.this.startActivity(intentMain);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (id == R.id.bestbooks) {
            Intent intentMain = new Intent(mostemailedarticles.this,
                    bestBooks.class);
            mostemailedarticles.this.startActivity(intentMain);


        }
        else if (id == R.id.share) {
            Intent intentMain = new Intent(mostemailedarticles.this,
                    search.class);
            mostemailedarticles.this.startActivity(intentMain);


        }

        return true;

    }







    private void aw() {

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Menu menu = navigationView.getMenu();

        MenuItem gallery = menu.findItem(R.id.nav_gallery);
        MenuItem slideshow = menu.findItem(R.id.nav_slideshow);
        MenuItem tools = menu.findItem(R.id.nav_tools);
        MenuItem books = menu.findItem(R.id.bestbooks);


        // set new title to the MenuItem
        gallery.setTitle("Most Emailed Articles1");
        slideshow.setTitle("Most Shared Articles");
        tools.setTitle("Most Viewed Articles");
        books.setTitle("BestBooks");
        MenuItem search = menu.findItem(R.id.share);

        search.setTitle("search books");

        navigationView.setNavigationItemSelectedListener(this);

    }
}


