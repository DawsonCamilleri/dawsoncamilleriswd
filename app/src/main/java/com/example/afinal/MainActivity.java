package com.example.afinal;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.multidots.fingerprintauth.AuthErrorCodes;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;

public class MainActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener,FingerPrintAuthCallback{
    // implements NavigationView.OnNavigationItemSelectedListener {
    FingerPrintAuthHelper mFingerPrintAuthHelper;

    private static final String TAG = "MainActivity";
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    Pinview pinview;
    RecyclerView recyclerView;
    private DrawerLayout drawer;
    public Snackbar mySnackbar;
    public Snackbar mySnackbar2;
    int permsRequestCode = 200;
    private EditText mName;
    private CheckBox mCheckbox;
    private Button mLogin;
    private Button txtbtn;
    private Toolbar toolbar;
    TextView mName2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);

        mName2 = (TextView) findViewById(R.id.d);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        aw();
        // setContentView(R.layout.activity_main);

        pinview = (Pinview) findViewById(R.id.mypinview);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {

            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                Toast.makeText(getApplicationContext(), pinview.getValue(), Toast.LENGTH_SHORT).show();

                if (pinview.getValue().equals("12345")) {
        ;



                  setContentView(R.layout.main);

                    aw();
                   toolbar = (Toolbar) findViewById(R.id.toolbar);
                   toolbar.setSubtitle("Home Page");
                 toolbar.inflateMenu(R.menu.menu_context);

                    toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            if(item.getItemId()==R.id.citm4)
                            {

                                String url = "https://www.nytimes.com/";

                                // Parse the URI and create the intent.
                                Uri webpage = Uri.parse(url);
                                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

                                // Find an activity to hand the intent and start that activity.
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                } else {
                                    Log.d("ImplicitIntents", "Can't handle this intent!");
                                }

                            }


                            return false;
                        }
                    });





                    ImageView img = (ImageView) findViewById(R.id.fab);
                    img.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri.parse("mailto:dawsoncamilleri@gmail.com?subject=" + "Queries" + "&body=" + "");
                            intent.setData(data);
                            startActivity(intent);            }
                    });








                    mName = (EditText) findViewById(R.id.etName);
                    mCheckbox = (CheckBox) findViewById(R.id.checkBox);
                    mLogin = (Button) findViewById(R.id.button);

                    mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


                    mEditor = mPreferences.edit();


                    checkSahredPreferences();






                    mLogin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (mCheckbox.isChecked()) {
                                mEditor.putString(getString(R.string.checkbox), "True");
                                mEditor.commit();
                                String name = mName.getText().toString();
                                mEditor.putString(getString(R.string.name), name);
                                mEditor.commit();


                            } else {
                                mEditor.putString(getString(R.string.checkbox), "");
                                mEditor.commit();
                                mEditor.putString(getString(R.string.name), "");
                                mEditor.commit();


                            }

                        }

                    });


                    // Initializing list view with the custom adapter
                //    ArrayList<Item> itemList = new ArrayList<>();

                //    ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(R.layout.list_item, itemList);
                //    recyclerView = (RecyclerView) findViewById(R.id.item_list);
                //    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                //    recyclerView.setItemAnimator(new DefaultItemAnimator());
                //    recyclerView.setAdapter(itemArrayAdapter);

                    // Populating list items
                  //  for (int i = 0; i < 5; i++) {
                  //      itemList.add(new Item("Item " + i));
                   // }


                }


            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        //start finger print authentication
        mFingerPrintAuthHelper.startAuth();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), formatter.format(date), Snackbar.LENGTH_SHORT);

        mySnackbar.show();



    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onPause() {
        super.onPause();
        mFingerPrintAuthHelper.stopAuth();
        String checkbox = mPreferences.getString(getString(R.string.checkbox), "False");
        String name = mPreferences.getString(getString(R.string.name), "");

        mName.setText(name);


        if (checkbox.equals("True")) {

            mCheckbox.setChecked(true);

        } else {
            mCheckbox.setChecked(false);


        }



        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());

        mEditor.putString(getString(R.string.date), formatter.format(date));
        mEditor.commit();


    }


    @Override
    public void onNoFingerPrintHardwareFound() {
        mName2.setText("no sensor found");

    }

    @Override
    public void onNoFingerPrintRegistered() {

    }

    @Override
    public void onBelowMarshmallow() {
        mName2.setText("not supported");

    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        setContentView(R.layout.main);
        aw();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setSubtitle("Home Page");
        toolbar.inflateMenu(R.menu.menu_context);




        mName = (EditText) findViewById(R.id.etName);
        mCheckbox = (CheckBox) findViewById(R.id.checkBox);
        mLogin = (Button) findViewById(R.id.button);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        mEditor = mPreferences.edit();


        checkSahredPreferences();


        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCheckbox.isChecked()) {
                    mEditor.putString(getString(R.string.checkbox), "True");
                    mEditor.commit();
                    String name = mName.getText().toString();
                    mEditor.putString(getString(R.string.name), name);
                    mEditor.commit();


                } else {
                    mEditor.putString(getString(R.string.checkbox), "");
                    mEditor.commit();
                    mEditor.putString(getString(R.string.name), "");
                    mEditor.commit();


                }

            }


        });



    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        switch (errorCode) {    //Parse the error code for recoverable/non recoverable error.
            case AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                //Cannot recognize the fingerprint scanned.
                mName2.setText("wrong");

                break;
            case AuthErrorCodes.NON_RECOVERABLE_ERROR:
                //This is not recoverable error. Try other options for user authentication. like pin, password.
                break;
            case AuthErrorCodes.RECOVERABLE_ERROR:
                //Any recoverable error. Display message to the user.
                break;
        }
    }

    String[] perms = {"android.permission.FINE_LOCATION", "android.permission.CAMERA"};


   // @Override
   @TargetApi(Build.VERSION_CODES.M)
   public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        switch(permsRequestCode){

            case 200:

                boolean locationAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                boolean cameraAccepted = grantResults[1]== PackageManager.PERMISSION_GRANTED;

                break;

        }

    }





    protected void onStart() {
        super.onStart();
   // showToast("hj",7);

     //   showToast2();


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), formatter.format(date), Snackbar.LENGTH_SHORT);

        mySnackbar.show();



    }

/*
  protected void onResume(){
       // super.onResume();



      showToast2();
   }

*/

/*
    public void onPause(){
        super.onPause();
        String checkbox = mPreferences.getString(getString(R.string.checkbox), "False");
        String name = mPreferences.getString(getString(R.string.name), "");


        mName.setText(name);


        if (checkbox.equals("True")) {

            mCheckbox.setChecked(true);

        } else {
            mCheckbox.setChecked(false);


        }

    }
*/
    protected void onStop(){
        super.onStop();

        String checkbox = mPreferences.getString(getString(R.string.checkbox), "False");
        String name = mPreferences.getString(getString(R.string.name), "");

        mName.setText(name);


        if (checkbox.equals("True")) {

            mCheckbox.setChecked(true);

        } else {
            mCheckbox.setChecked(false);


        }


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());

        mEditor.putString(getString(R.string.date), formatter.format(date));
        mEditor.commit();




    }




    private void checkSahredPreferences() {
        String checkbox = mPreferences.getString(getString(R.string.checkbox), "False");
        String name = mPreferences.getString(getString(R.string.name), "");


        mName.setText(name);


        if (checkbox.equals("True")) {

            mCheckbox.setChecked(true);

        } else {
            mCheckbox.setChecked(false);


        }

    }


    private void showToast(CharSequence msg, int duration) {
        Toast toast = Toast.makeText(getApplicationContext(), "jhh", duration);
        toast.show();
    }

    private void aw() {

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        Menu menu = navigationView.getMenu();

        MenuItem gallery = menu.findItem(R.id.nav_gallery);
        MenuItem slideshow = menu.findItem(R.id.nav_slideshow);
        MenuItem tools = menu.findItem(R.id.nav_tools);

        MenuItem books = menu.findItem(R.id.bestbooks);
        MenuItem search = menu.findItem(R.id.share);

        // set new title to the MenuItem
        gallery.setTitle("Most Emailed Articles");
        slideshow.setTitle("Most Shared Articles");
        tools.setTitle("Most Viewed Articles");
        books.setTitle("Best books");

        search.setTitle("search books");





    }

    //Options Menu
   @Override
  public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.menu_context, menu);

        return true;
   }





@Override
    public boolean onContextItemSelected(MenuItem item){
           switch(item.getItemId()){
               case R.id.citm4:
                   String url = "https://www.nytimes.com/";

                   // Parse the URI and create the intent.
                   Uri webpage = Uri.parse(url);
                   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

                   // Find an activity to hand the intent and start that activity.
                   if (intent.resolveActivity(getPackageManager()) != null) {
                       startActivity(intent);
                   } else {
                       Log.d("ImplicitIntents", "Can't handle this intent!");
                   }
                   break;

             default: return false;

         }
           return true;

         }



    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            // setTitle("#");
            //  first p = new first();
            // FragmentManager frag = getSupportFragmentManager();
            //   frag.beginTransaction().replace(R.id.fragment,p).commit();
            showToast("popup 2", Toast.LENGTH_SHORT);
            Intent intentMain = new Intent(MainActivity.this,
                    MainActivity.class);
            MainActivity.this.startActivity(intentMain);


        } else if (id == R.id.nav_gallery) {

            showToast("popup 2", Toast.LENGTH_SHORT);
            Intent intentMain = new Intent(MainActivity.this,
                    MostSharedArticles.class);
            MainActivity.this.startActivity(intentMain);

        } else if (id == R.id.nav_slideshow) {
            Intent intentMain = new Intent(MainActivity.this,
                    mostemailedarticles.class);
            MainActivity.this.startActivity(intentMain);

        }
        else if (id == R.id.bestbooks) {
            Intent intentMain = new Intent(MainActivity.this,
                    bestBooks.class);
            MainActivity.this.startActivity(intentMain);


        }
        else if (id == R.id.share) {
            Intent intentMain = new Intent(MainActivity.this,
                    search.class);
            MainActivity.this.startActivity(intentMain);


        }
        else if (id == R.id.nav_tools) {
            Intent intentMain = new Intent(MainActivity.this,
                    mostviewedarticles.class);
            MainActivity.this.startActivity(intentMain);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;



    }









}









