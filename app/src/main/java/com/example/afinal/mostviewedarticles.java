package com.example.afinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class mostviewedarticles extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    private Toolbar toolbar;

    RecyclerView recyclerView;
    RecyclerView recyclerView2;

    String JASON = "{  \n" +
            "   \"status\":\"OK\",\n" +
            "   \"copyright\":\"Copyright (c) 2019 The New York Times Company.  All Rights Reserved.\",\n" +
            "   \"num_results\":1504,\n" +
            "   \"results\":[  \n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/style\\/generation-xers.html\",\n" +
            "         \"adx_keywords\":\"Music;Pop and Rock Music;Television;Fashion and Apparel;Magazines;Smoking and Tobacco;Lollapalooza (Music Festival);Nineteen Hundred Nineties;Benetton Group SpA;Dr Martens;Drug Abuse Resistance Education (DARE);Living Colour (Music Group);Parents Music Resource Center;TLC (Music Group);Bonet, Lisa;Cobain, Kurt;Gore, Tipper;Kravitz, Lenny;Reeves, Keanu;Prada, Miuccia;Twin Peaks (TV Program);Generation X\",\n" +
            "         \"column\":\"\",\n" +
            "         \"section\":\"Style\",\n" +
            "         \"byline\":\"\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Gen X Is a Mess\",\n" +
            "         \"abstract\":\"The great generation that barely was.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006490125,\n" +
            "         \"asset_id\":100000006490125,\n" +
            "         \"views\":1,\n" +
            "         \"des_facet\":[  \n" +
            "            \"MUSIC\",\n" +
            "            \"POP AND ROCK MUSIC\",\n" +
            "            \"TELEVISION\",\n" +
            "            \"GENERATION X\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"FASHION AND APPAREL\",\n" +
            "            \"MAGAZINES\",\n" +
            "            \"SMOKING AND TOBACCO\",\n" +
            "            \"LOLLAPALOOZA (MUSIC FESTIVAL)\",\n" +
            "            \"NINETEEN HUNDRED NINETIES\",\n" +
            "            \"BENETTON GROUP SPA\",\n" +
            "            \"DR MARTENS\",\n" +
            "            \"DRUG ABUSE RESISTANCE EDUCATION (DARE)\",\n" +
            "            \"LIVING COLOUR (MUSIC GROUP)\",\n" +
            "            \"PARENTS MUSIC RESOURCE CENTER\",\n" +
            "            \"TLC (MUSIC GROUP)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BONET, LISA\",\n" +
            "            \"COBAIN, KURT\",\n" +
            "            \"GORE, TIPPER\",\n" +
            "            \"KRAVITZ, LENNY\",\n" +
            "            \"REEVES, KEANU\",\n" +
            "            \"PRADA, MIUCCIA\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Lenny Kravitz and Lisa Bonet\",\n" +
            "               \"copyright\":\"Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/fashion\\/14genx-generation-xers-promo-STILL\\/8c73df20ff1f4aaa8c53806dba72ad30-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/f2717793-1a5c-581f-8d8c-0a2dde2e2dcc\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/16\\/obituaries\\/im-pei-dead.html\",\n" +
            "         \"adx_keywords\":\"Pei, I M;Architecture;Deaths (Obituaries);China;Museums;Real Estate (Commercial);Louvre Museum;National Gallery of Art;Kennedy, John F, Library and Museum\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Obituaries\",\n" +
            "         \"byline\":\"By PAUL GOLDBERGER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"I.M. Pei, Master Architect Whose Buildings Dazzled the World, Dies at 102\",\n" +
            "         \"abstract\":\"Mr. Pei, a committed modernist, was one of the few architects equally attractive to real estate developers, corporate chieftains and art museum boards.\",\n" +
            "         \"published_date\":\"2019-05-16\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000004447974,\n" +
            "         \"asset_id\":100000004447974,\n" +
            "         \"views\":2,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ARCHITECTURE\",\n" +
            "            \"DEATHS (OBITUARIES)\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"MUSEUMS\",\n" +
            "            \"REAL ESTATE (COMMERCIAL)\",\n" +
            "            \"LOUVRE MUSEUM\",\n" +
            "            \"NATIONAL GALLERY OF ART\",\n" +
            "            \"KENNEDY, JOHN F, LIBRARY AND MUSEUM\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"PEI, I M\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"CHINA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"I.M. Pei in 1989 outside the glass pyramid he designed at the Louvre in Paris, one of his most famous commissions. &ldquo;If there&rsquo;s one thing I know I didn&rsquo;t do wrong, it&rsquo;s the Louvre,&rdquo; he said.\",\n" +
            "               \"copyright\":\"Marc Riboud\\/Magnum Photos\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/obituaries\\/17Pei-obit-p11\\/16Pei-obit1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/obituaries\\/17Pei-obit-p11\\/75a2b273b7894a5bb25a46905995f78d-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/obituaries\\/17Pei-obit-p11\\/75a2b273b7894a5bb25a46905995f78d-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b7f012cb-0822-5464-bbf0-fad80495d489\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/16\\/arts\\/television\\/binge-watch-game-of-thrones.html\",\n" +
            "         \"adx_keywords\":\"Television;Game of Thrones (TV Program);Home Box Office;Martin, George R R;A Song of Ice and Fire (Book)\",\n" +
            "         \"column\":\"Critic\\u2019s Notebook\",\n" +
            "         \"section\":\"Arts\",\n" +
            "         \"byline\":\"By WESLEY MORRIS\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"I Ignored \\u2018Game of Thrones\\u2019 for 8 Years. Then Inhaled It in 5 Weeks.\",\n" +
            "         \"abstract\":\"For more than 70 hours, Wesley Morris caught up with the dragons, beheadings and brinkmanship before the series finale. Here\\u2019s what he learned.\",\n" +
            "         \"published_date\":\"2019-05-16\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509524,\n" +
            "         \"asset_id\":100000006509524,\n" +
            "         \"views\":3,\n" +
            "         \"des_facet\":[  \n" +
            "            \"TELEVISION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"HOME BOX OFFICE\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"MARTIN, GEORGE R R\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Photographs by HBO\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16GOTwesley-combo-promo\\/16GOTwesley-combo-promo-thumbStandard-v3.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16GOTwesley-combo-promo\\/16GOTwesley-combo-promo-mediumThreeByTwo210-v3.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16GOTwesley-combo-promo\\/16GOTwesley-combo-promo-mediumThreeByTwo440-v3.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/33a60b6b-ab4f-510e-ae2e-ded6bd95a2e2\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/16\\/world\\/middleeast\\/iran-war-donald-trump.html\",\n" +
            "         \"adx_keywords\":\"United States International Relations;United States Defense and Military Forces;United States Politics and Government;Defense Department;Bolton, John R;Pompeo, Mike;Shanahan, Patrick M (1962- );Trump, Donald J;Iran\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"World\",\n" +
            "         \"byline\":\"By MARK LANDLER, MAGGIE HABERMAN and ERIC SCHMITT\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Trump Tells Pentagon Chief He Does Not Want War With Iran\",\n" +
            "         \"abstract\":\"The president\\u2019s statement came during a briefing on the rising tensions with Tehran, and officials said he was firm in saying he did not want a military clash.\",\n" +
            "         \"published_date\":\"2019-05-16\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006512849,\n" +
            "         \"asset_id\":100000006512849,\n" +
            "         \"views\":4,\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES INTERNATIONAL RELATIONS\",\n" +
            "            \"UNITED STATES DEFENSE AND MILITARY FORCES\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"DEFENSE DEPARTMENT\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BOLTON, JOHN R\",\n" +
            "            \"POMPEO, MIKE\",\n" +
            "            \"SHANAHAN, PATRICK M (1962- )\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"IRAN\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"President Trump told Patrick Shanahan, second from left, the acting defense secretary, that he does not want to go to war with Iran.\",\n" +
            "               \"copyright\":\"Sarah Silbiger\\/The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/us\\/politics\\/16dc-prexy-promo\\/16dc-prexy-promo-thumbStandard-v5.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/us\\/politics\\/16dc-prexy-promo\\/16dc-prexy-promo-mediumThreeByTwo210-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/us\\/politics\\/16dc-prexy-promo\\/16dc-prexy-promo-mediumThreeByTwo440-v6.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/8024351e-1ec3-586a-924f-369a62650931\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/style\\/james-charles-makeup-artist-youtube.html\",\n" +
            "         \"adx_keywords\":\"Social Media;Fashion and Apparel;Cosmetics and Toiletries;Charles, James (Makeup Artist);Westbrook, Tati;YouTube.com\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"byline\":\"By VALERIYA SAFRONOVA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"James Charles, From \\u2018CoverBoy\\u2019 to Canceled\",\n" +
            "         \"abstract\":\"The 19-year-old internet personality and makeup artist has provoked the ire of beauty YouTube.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509205,\n" +
            "         \"asset_id\":100000006509205,\n" +
            "         \"views\":5,\n" +
            "         \"des_facet\":[  \n" +
            "            \"SOCIAL MEDIA\",\n" +
            "            \"FASHION AND APPAREL\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"COSMETICS AND TOILETRIES\",\n" +
            "            \"YOUTUBE.COM\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"CHARLES, JAMES (MAKEUP ARTIST)\",\n" +
            "            \"WESTBROOK, TATI\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"James Charles at Gucci&rsquo;s Met Gala after-party on May 6. In the week since, he lost millions of YouTube subscribers after another influencer, Tati Westbrook, shared a critical video about him.\",\n" +
            "               \"copyright\":\"Krista Schlueter for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/14Jamescharles-met-gala-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/merlin_154504278_191f95fd-0be9-4b5b-84fa-bca67f4730a9-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/fashion\\/14Jamescharles-met-gala\\/merlin_154504278_191f95fd-0be9-4b5b-84fa-bca67f4730a9-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/f0598a1b-04ca-5a5c-8aff-528f6036924d\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/world\\/middleeast\\/iran-war-usa.html\",\n" +
            "         \"adx_keywords\":\"Iran;Defense and Military Forces;Islamic Revolutionary Guards Corps;United States International Relations;United States Defense and Military Forces;United States Politics and Government;Diplomatic Service, Embassies and Consulates;Iraq War (2003-11);Espionage and Intelligence Services;Missiles and Missile Defense Systems;State Department;Bolton, John R;Pompeo, Mike;Trump, Donald J\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"World\",\n" +
            "         \"byline\":\"By JULIAN E. BARNES, ERIC SCHMITT, NICHOLAS FANDOS and EDWARD WONG\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Iran Threat Debate Is Set Off by Images of Missiles at Sea\",\n" +
            "         \"abstract\":\"The photographs fueled fears that Iran would fire missiles at U.S. naval ships in the Persian Gulf. But some said the moves could be defensive acts against Washington provocations.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006510613,\n" +
            "         \"asset_id\":100000006510613,\n" +
            "         \"views\":6,\n" +
            "         \"des_facet\":[  \n" +
            "            \"UNITED STATES INTERNATIONAL RELATIONS\",\n" +
            "            \"UNITED STATES DEFENSE AND MILITARY FORCES\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"DEFENSE AND MILITARY FORCES\",\n" +
            "            \"ISLAMIC REVOLUTIONARY GUARDS CORPS\",\n" +
            "            \"DIPLOMATIC SERVICE, EMBASSIES AND CONSULATES\",\n" +
            "            \"IRAQ WAR (2003-11)\",\n" +
            "            \"ESPIONAGE AND INTELLIGENCE SERVICES\",\n" +
            "            \"MISSILES AND MISSILE DEFENSE SYSTEMS\",\n" +
            "            \"STATE DEPARTMENT\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"BOLTON, JOHN R\",\n" +
            "            \"POMPEO, MIKE\",\n" +
            "            \"TRUMP, DONALD J\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"IRAN\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"President Hassan Rouhani of Iran, center, and military commanders during National Army Day in Tehran last month.\",\n" +
            "               \"copyright\":\"Abedin Taherkenareh\\/EPA, via Shutterstock\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/06\\/15\\/world\\/15dc-intel-promo\\/15dc-intel-promo-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/06\\/15\\/world\\/15dc-intel-promo\\/15dc-intel-promo-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/06\\/15\\/world\\/15dc-intel-promo\\/15dc-intel-promo-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/9c250bbe-1e73-58e6-87c7-952a0c4977cf\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/magazine\\/cbd-cannabis-cure.html\",\n" +
            "         \"adx_keywords\":\"2019 Health Issue;Cannabis Foods and Products;Seizures (Medical);Medical Marijuana;Marijuana\",\n" +
            "         \"column\":\"\",\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"byline\":\"By MOISES VELASQUEZ-MANOFF\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Can CBD Really Do All That?\",\n" +
            "         \"abstract\":\"How one molecule from the cannabis plant came to be seen as a therapeutic cure-all.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006504038,\n" +
            "         \"asset_id\":100000006504038,\n" +
            "         \"views\":7,\n" +
            "         \"des_facet\":[  \n" +
            "            \"CANNABIS FOODS AND PRODUCTS\",\n" +
            "            \"MARIJUANA\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"SEIZURES (MEDICAL)\",\n" +
            "            \"MEDICAL MARIJUANA\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Various products that have been advertised as containing CBD.\",\n" +
            "               \"copyright\":\"Jamie Chung for The New York Times. Prop styling by Anna Surbatovich.\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19mag-CBD-02\\/19mag-CBD-02-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/4910bb4b-d110-5dc7-bb7c-712df5be2de3\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/us\\/abortion-laws-states.html\",\n" +
            "         \"adx_keywords\":\"Abortion;Alabama;Women's Rights;Medicine and Health;Pregnancy and Childbirth\",\n" +
            "         \"column\":\"\",\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By K.K. REBECCA LAI\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Abortion Bans: How State Laws Have Limited the Procedure This Year\",\n" +
            "         \"abstract\":\"Alabama legislators on Tuesday voted to ban abortions in nearly all cases, making the state the seventh this year to pass restrictions on when abortions are allowed.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006510571,\n" +
            "         \"asset_id\":100000006510571,\n" +
            "         \"views\":8,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"WOMEN'S RIGHTS\",\n" +
            "            \"MEDICINE AND HEALTH\",\n" +
            "            \"PREGNANCY AND CHILDBIRTH\"\n" +
            "         ],\n" +
            "         \"org_facet\":\"\",\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/abortion-ban-promo-1557933612354\\/abortion-ban-promo-1557933612354-thumbStandard-v8.png\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/abortion-ban-promo-1557933612354\\/abortion-ban-promo-1557933612354-mediumThreeByTwo210-v8.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/abortion-ban-promo-1557933612354\\/abortion-ban-promo-1557933612354-mediumThreeByTwo440-v8.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/c2def340-4a0b-5a24-824b-88e33871909e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/us\\/abortion-laws-2019.html\",\n" +
            "         \"adx_keywords\":\"Abortion;State Legislatures;Law and Legislation;Politics and Government;Roe v Wade (Supreme Court Decision);Supreme Court (US);Alabama;Ivey, Kay;Illinois\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By SABRINA TAVERNISE\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"\\u2018The Time Is Now\\u2019: States Are Rushing to Restrict Abortion, or to Protect It\",\n" +
            "         \"abstract\":\"States across the country are passing some of the most restrictive abortion laws in decades, including in Alabama, where Gov. Kay Ivey signed a bill effectively banning the procedure.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006511114,\n" +
            "         \"asset_id\":100000006511114,\n" +
            "         \"views\":9,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"LAW AND LEGISLATION\",\n" +
            "            \"POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"STATE LEGISLATURES\",\n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\",\n" +
            "            \"SUPREME COURT (US)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"IVEY, KAY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\",\n" +
            "            \"ILLINOIS\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Gov. Kay Ivey of Alabama signed a bill that virtually outlaws abortion in the state on Wednesday.\",\n" +
            "               \"copyright\":\"Blake Paterson\\/Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15ABORTION-HPPROMO-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15US-Briefing-PM-slide-72E3-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/briefing\\/15ABORTION-governor\\/15US-Briefing-PM-slide-72E3-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/62555284-fece-518a-8d7c-58c0c743738a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/us\\/facial-recognition-ban-san-francisco.html\",\n" +
            "         \"adx_keywords\":\"San Francisco (Calif);Computer Vision;Face;Surveillance of Citizens by Government;Privacy;Politics and Government;Police\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By KATE CONGER, RICHARD FAUSSET and SERGE F. KOVALESKI\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"San Francisco Bans Facial Recognition Technology\",\n" +
            "         \"abstract\":\"It is the first ban by a major city on the use of facial recognition technology by the police and all other municipal agencies.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509114,\n" +
            "         \"asset_id\":100000006509114,\n" +
            "         \"views\":10,\n" +
            "         \"des_facet\":[  \n" +
            "            \"COMPUTER VISION\",\n" +
            "            \"FACE\",\n" +
            "            \"SURVEILLANCE OF CITIZENS BY GOVERNMENT\",\n" +
            "            \"POLICE\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"PRIVACY\",\n" +
            "            \"POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"SAN FRANCISCO (CALIF)\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Attendees interacting with a facial recognition demonstration at this year\\u2019s CES in Las Vegas.\",\n" +
            "               \"copyright\":\"Joe Buglewicz for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/14\\/us\\/14facialrecognition-01\\/14facialrecognition-01-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/b6c5a8d0-5c8f-573a-ad47-8829c6c5fc38\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/16\\/magazine\\/how-much-alcohol-can-you-drink-safe-health.html\",\n" +
            "         \"adx_keywords\":\"Alcoholic Beverages;Alcohol Abuse;Research;Academic and Scientific Journals;Lancet, The (Journal)\",\n" +
            "         \"column\":\"Studies Show\",\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"byline\":\"By KIM TINGLEY\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"How Much Alcohol Can You Drink Safely?\",\n" +
            "         \"abstract\":\"That depends on how you do the research \\u2014 and then on how you read the results. Here\\u2019s what studies show.\",\n" +
            "         \"published_date\":\"2019-05-16\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006503859,\n" +
            "         \"asset_id\":100000006503859,\n" +
            "         \"views\":11,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ALCOHOLIC BEVERAGES\",\n" +
            "            \"ALCOHOL ABUSE\",\n" +
            "            \"RESEARCH\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ACADEMIC AND SCIENTIFIC JOURNALS\",\n" +
            "            \"LANCET, THE (JOURNAL)\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"\",\n" +
            "               \"copyright\":\"Illustration by Celia Jacobs\",\n" +
            "               \"approved_for_syndication\":0,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Studies_01\\/42ae82a8ab644d10a60cc719da368d7c-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Studies_01\\/42ae82a8ab644d10a60cc719da368d7c-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Studies_01\\/42ae82a8ab644d10a60cc719da368d7c-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/e4cbb3e2-5284-5e75-9d51-08cfe18cdd9a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/14\\/us\\/abortion-law-alabama.html\",\n" +
            "         \"adx_keywords\":\"Abortion;Alabama;Roe v Wade (Supreme Court Decision);Alabama Pro-Life Coalition;Law and Legislation;State Legislatures;Women's Rights;Supreme Court (US);Johnston, Eric (Attorney)\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By TIMOTHY WILLIAMS and ALAN BLINDER\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Lawmakers Vote to Effectively Ban Abortion in Alabama\",\n" +
            "         \"abstract\":\"Supporters of the measure hope it will lead the Supreme Court to reconsider Roe v. Wade, which recognized a constitutional right to end a pregnancy.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006501413,\n" +
            "         \"asset_id\":100000006501413,\n" +
            "         \"views\":12,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"LAW AND LEGISLATION\",\n" +
            "            \"STATE LEGISLATURES\",\n" +
            "            \"WOMEN'S RIGHTS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ROE V WADE (SUPREME COURT DECISION)\",\n" +
            "            \"ALABAMA PRO-LIFE COALITION\",\n" +
            "            \"SUPREME COURT (US)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"JOHNSTON, ERIC (ATTORNEY)\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Demonstrators dressed as handmaids protested at the Alabama State House in April against the proposed abortion ban.\",\n" +
            "               \"copyright\":\"Mickey Welsh\\/The Montgomery Advertiser, via Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-mediumThreeByTwo210-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/09\\/us\\/09ABORTION-1\\/09ABORTION-1-mediumThreeByTwo440-v2.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/38f3c228-7789-5f4b-88f0-6bb61c99599e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/16\\/us\\/sat-score.html\",\n" +
            "         \"adx_keywords\":\"Admissions Standards;SAT (College Admission Test);Tests and Examinations;College Board;Affirmative Action;Colleges and Universities\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By ANEMONA HARTOCOLLIS\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"SAT\\u2019s New \\u2018Adversity Score\\u2019 Will Take Students\\u2019 Hardships Into Account\",\n" +
            "         \"abstract\":\"The College Board is adding a measure of students\\u2019 socioeconomic background to help colleges put test scores in context.\",\n" +
            "         \"published_date\":\"2019-05-16\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006512824,\n" +
            "         \"asset_id\":100000006512824,\n" +
            "         \"views\":13,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ADMISSIONS STANDARDS\",\n" +
            "            \"SAT (COLLEGE ADMISSION TEST)\",\n" +
            "            \"TESTS AND EXAMINATIONS\",\n" +
            "            \"AFFIRMATIVE ACTION\",\n" +
            "            \"COLLEGES AND UNIVERSITIES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"COLLEGE BOARD\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Scoring patterns on the SAT suggesting that the test puts certain racial and economic groups at a disadvantage have become a concern for colleges.\",\n" +
            "               \"copyright\":\"Shiho Fukada for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/us\\/17-adversityscore-PRINT\\/16adversityscore1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/us\\/17-adversityscore-PRINT\\/merlin_103778194_0951d72c-7c23-44ea-b195-8b88f74c73ab-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/17\\/us\\/17-adversityscore-PRINT\\/merlin_103778194_0951d72c-7c23-44ea-b195-8b88f74c73ab-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/ba55947f-0054-5744-8346-460e036c319f\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/technology\\/uber-ipo-price.html\",\n" +
            "         \"adx_keywords\":\"Car Services and Livery Cabs;Initial Public Offerings;Stocks and Bonds;Banking and Financial Institutions;Uber Technologies Inc;Khosrowshahi, Dara;Goldman Sachs Group Inc;Morgan Stanley;Lyft Inc\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Technology\",\n" +
            "         \"byline\":\"By MIKE ISAAC, MICHAEL J. de la MERCED and ANDREW ROSS SORKIN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"How the Promise of a $120 Billion Uber I.P.O. Evaporated\",\n" +
            "         \"abstract\":\"Uber\\u2019s offering was supposed to be a crowning moment for the ride-hailing company. But it suffered setback after setback and ultimately resulted in pointed questions for all involved.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509452,\n" +
            "         \"asset_id\":100000006509452,\n" +
            "         \"views\":14,\n" +
            "         \"des_facet\":[  \n" +
            "            \"CAR SERVICES AND LIVERY CABS\",\n" +
            "            \"INITIAL PUBLIC OFFERINGS\",\n" +
            "            \"STOCKS AND BONDS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"BANKING AND FINANCIAL INSTITUTIONS\",\n" +
            "            \"UBER TECHNOLOGIES INC\",\n" +
            "            \"GOLDMAN SACHS GROUP INC\",\n" +
            "            \"MORGAN STANLEY\",\n" +
            "            \"LYFT INC\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"KHOSROWSHAHI, DARA\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Uber signage decorated the exterior of the New York Stock Market last week.\",\n" +
            "               \"copyright\":\"Jeenah Moon for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/business\\/00uber2\\/00uber2-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/business\\/00uber2\\/merlin_154655346_f32c8c76-07fb-4907-8b9e-59ed4a3043d0-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/business\\/00uber2\\/merlin_154655346_f32c8c76-07fb-4907-8b9e-59ed4a3043d0-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/f616bd42-df61-548b-8aaa-6b32adf8f943\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/style\\/busy-philipps-abortion-youknowme.html\",\n" +
            "         \"adx_keywords\":\"Abortion;Philipps, Busy;Alabama;Busy Tonight (TV Program);Television;Social Media\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Style\",\n" +
            "         \"byline\":\"By VALERIYA SAFRONOVA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Thousands of Women Have Shared Abortion Stories With #YouKnowMe. She Was First.\",\n" +
            "         \"abstract\":\"After Busy Philipps opened up about her abortion on TV, a friend saw an opportunity for a bigger conversation about reproductive rights.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006511207,\n" +
            "         \"asset_id\":100000006511207,\n" +
            "         \"views\":15,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ABORTION\",\n" +
            "            \"TELEVISION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"SOCIAL MEDIA\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"PHILIPPS, BUSY\"\n" +
            "         ],\n" +
            "         \"geo_facet\":[  \n" +
            "            \"ALABAMA\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Busy Philipps in 2018. The actress spoke about her abortion, at age 15, on her late-night show, \\u201cBusy Tonight,\\u201d then took her story to Twitter.\",\n" +
            "               \"copyright\":\"Tawni Bannister for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/fashion\\/15youknowme2\\/15youknowme2-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/4dafaff7-7752-5fd1-99c5-a593bd7d237e\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/opinion\\/race-identity\\/harvard-law-harvey-weinstein.html\",\n" +
            "         \"adx_keywords\":\"Sullivan, Ronald S Jr;Harvard Law School;Harvard University;Weinstein, Harvey;Khurana, Rakesh;Colleges and Universities;Law Schools;#MeToo Movement;Legal Profession\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Opinion\",\n" +
            "         \"byline\":\"By RANDALL KENNEDY\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Harvard Betrays a Law Professor \\u2014 and Itself\",\n" +
            "         \"abstract\":\"Misguided students believe that defending Harvey Weinstein makes Ronald Sullivan unfit to be their dean. Apparently the university agrees.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509000,\n" +
            "         \"asset_id\":100000006509000,\n" +
            "         \"views\":16,\n" +
            "         \"des_facet\":[  \n" +
            "            \"COLLEGES AND UNIVERSITIES\",\n" +
            "            \"#METOO MOVEMENT\",\n" +
            "            \"LEGAL PROFESSION\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"HARVARD LAW SCHOOL\",\n" +
            "            \"HARVARD UNIVERSITY\",\n" +
            "            \"LAW SCHOOLS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"SULLIVAN, RONALD S JR\",\n" +
            "            \"WEINSTEIN, HARVEY\",\n" +
            "            \"KHURANA, RAKESH\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Ronald Sullivan, left, arriving at State Supreme Court in Manhattan with his client Harvey Weinstein, third from left, in January.\",\n" +
            "               \"copyright\":\"Timothy A. Clary\\/Agence France-Presse \\u2014 Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/opinion\\/15Kennedy\\/15Kennedy-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/443464b8-4c73-5152-bfca-0bfad7d14e8a\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/us\\/georgetown-expels-students.html\",\n" +
            "         \"adx_keywords\":\"College Admissions Scandal (2019);Georgetown University;Semprevivo, Adam;Suits and Litigation (Civil);Colleges and Universities;Admissions Standards\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By KATE TAYLOR and JENNIFER MEDINA\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"His Father Paid $400,000 to Get Him Into Georgetown. Now He\\u2019s Suing the School.\",\n" +
            "         \"abstract\":\"Two months after the scandal broke, Georgetown said it planned to dismiss two students. One is suing, saying the university denied him due process.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006509919,\n" +
            "         \"asset_id\":100000006509919,\n" +
            "         \"views\":17,\n" +
            "         \"des_facet\":[  \n" +
            "            \"COLLEGES AND UNIVERSITIES\",\n" +
            "            \"ADMISSIONS STANDARDS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"COLLEGE ADMISSIONS SCANDAL (2019)\",\n" +
            "            \"GEORGETOWN UNIVERSITY\",\n" +
            "            \"SUITS AND LITIGATION (CIVIL)\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"SEMPREVIVO, ADAM\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Stephen Semprevivo has pleaded guilty in the college admissions case.\",\n" +
            "               \"copyright\":\"Steven Senne\\/Associated Press\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/00georgetown1\\/00georgetown1-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/00georgetown1\\/00georgetown1-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/00georgetown1\\/00georgetown1-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/9bfdc940-54fd-534b-9173-2e3f078e2751\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/interactive\\/2019\\/05\\/14\\/magazine\\/monkeys-puerto-rico-trauma.html\",\n" +
            "         \"adx_keywords\":\"2019 Health Issue;Puerto Rico;Hurricane Maria (2017);Animal Behavior;Psychology and Psychologists;Monkeys and Apes;Research\",\n" +
            "         \"column\":\"\",\n" +
            "         \"section\":\"Magazine\",\n" +
            "         \"byline\":\"By LUKE DITTRICH\",\n" +
            "         \"type\":\"Interactive\",\n" +
            "         \"title\":\"Primal Fear: Can Monkeys Help Unlock the Secrets of Trauma?\",\n" +
            "         \"abstract\":\"Hurricane Maria devastated Puerto Rico\\u2019s \\u201cmonkey island.\\u201d The surviving primates could help scientists learn about the psychological response to traumatizing events.\",\n" +
            "         \"published_date\":\"2019-05-14\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006507531,\n" +
            "         \"asset_id\":100000006507531,\n" +
            "         \"views\":18,\n" +
            "         \"des_facet\":[  \n" +
            "            \"HURRICANE MARIA (2017)\",\n" +
            "            \"ANIMAL BEHAVIOR\",\n" +
            "            \"MONKEYS AND APES\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"PSYCHOLOGY AND PSYCHOLOGISTS\",\n" +
            "            \"RESEARCH\"\n" +
            "         ],\n" +
            "         \"per_facet\":\"\",\n" +
            "         \"geo_facet\":[  \n" +
            "            \"PUERTO RICO\"\n" +
            "         ],\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Rhesus macaques on Cayo Santiago, which is just to the southeast of the main part of Puerto Rico and was hit hard by Hurricane Maria.\",\n" +
            "               \"copyright\":\"Glenna Gordon for The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Monkeys1-2\\/19Monkeys1-07-thumbStandard.png\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Monkeys1-2\\/19Monkeys1-07-mediumThreeByTwo210.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/19\\/magazine\\/19Monkeys1-2\\/19Monkeys1-07-mediumThreeByTwo440.png\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/interactive\\/410db841-cbf0-56d8-9a6d-6318ba59dd40\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/arts\\/jeff-koons-rabbit-auction.html\",\n" +
            "         \"adx_keywords\":\"Art;Sculpture;Auctions;Gagosian Gallery;Christie's;Koons, Jeff;Newhouse, S I Jr;Hockney, David\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"Arts\",\n" +
            "         \"byline\":\"By SCOTT REYBURN\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Jeff Koons \\u2018Rabbit\\u2019 Sets Auction Record for Most Expensive Work by Living Artist\",\n" +
            "         \"abstract\":\"A shiny and enigmatic steel sculpture sold for $91.1 million at Christie\\u2019s on Wednesday, edging past David Hockney.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006510395,\n" +
            "         \"asset_id\":100000006510395,\n" +
            "         \"views\":19,\n" +
            "         \"des_facet\":[  \n" +
            "            \"ART\",\n" +
            "            \"SCULPTURE\",\n" +
            "            \"AUCTIONS\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"GAGOSIAN GALLERY\",\n" +
            "            \"CHRISTIE'S\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"KOONS, JEFF\",\n" +
            "            \"NEWHOUSE, S I JR\",\n" +
            "            \"HOCKNEY, DAVID\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Jeff Koons\\u2019s \\u201cRabbit\\u201d in stainless steel, sold at Christie\\u2019s New York for $91.1 million.\",\n" +
            "               \"copyright\":\"Timothy A. Clary\\/Agence France-Presse \\u2014 Getty Images\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16auction1-promo\\/16auction1-promo-thumbStandard-v2.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16auction1-promo\\/16auction1-promo-mediumThreeByTwo210-v4.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/16\\/arts\\/16auction1-promo\\/16auction1-promo-mediumThreeByTwo440-v4.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/7746c23b-9f14-5b47-9f41-f3d5f6194d9c\"\n" +
            "      },\n" +
            "      {  \n" +
            "         \"url\":\"https:\\/\\/www.nytimes.com\\/2019\\/05\\/15\\/us\\/politics\\/trump-immigration-kushner.html\",\n" +
            "         \"adx_keywords\":\"Trump, Donald J;Immigration and Emigration;Foreign Workers;Asylum, Right of;Deferred Action for Childhood Arrivals;Border Barriers;United States Politics and Government;Kushner, Jared\",\n" +
            "         \"column\":null,\n" +
            "         \"section\":\"U.S.\",\n" +
            "         \"byline\":\"By MICHAEL D. SHEAR\",\n" +
            "         \"type\":\"Article\",\n" +
            "         \"title\":\"Trump Immigration Proposal Emphasizes Immigrants\\u2019 Skills Over Family Ties\",\n" +
            "         \"abstract\":\"The plan, to be unveiled Thursday, will significantly scale back family-based immigration and increase the educational and skills requirements to move to the United States.\",\n" +
            "         \"published_date\":\"2019-05-15\",\n" +
            "         \"source\":\"The New York Times\",\n" +
            "         \"id\":100000006511363,\n" +
            "         \"asset_id\":100000006511363,\n" +
            "         \"views\":20,\n" +
            "         \"des_facet\":[  \n" +
            "            \"IMMIGRATION AND EMIGRATION\",\n" +
            "            \"FOREIGN WORKERS\",\n" +
            "            \"UNITED STATES POLITICS AND GOVERNMENT\"\n" +
            "         ],\n" +
            "         \"org_facet\":[  \n" +
            "            \"ASYLUM, RIGHT OF\",\n" +
            "            \"DEFERRED ACTION FOR CHILDHOOD ARRIVALS\",\n" +
            "            \"BORDER BARRIERS\"\n" +
            "         ],\n" +
            "         \"per_facet\":[  \n" +
            "            \"TRUMP, DONALD J\",\n" +
            "            \"KUSHNER, JARED\"\n" +
            "         ],\n" +
            "         \"geo_facet\":\"\",\n" +
            "         \"media\":[  \n" +
            "            {  \n" +
            "               \"type\":\"image\",\n" +
            "               \"subtype\":\"photo\",\n" +
            "               \"caption\":\"Jared Kushner, President Trump\\u2019s son-in-law and senior adviser, spent months working on the plan, which will be a central part of Mr. Trump\\u2019s re-election campaign message.\",\n" +
            "               \"copyright\":\"Erin Schaff\\/The New York Times\",\n" +
            "               \"approved_for_syndication\":1,\n" +
            "               \"media-metadata\":[  \n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15dc-immig-sub\\/merlin_154830909_a2ae6585-40ea-4aa6-91b7-d30898485f7f-thumbStandard.jpg\",\n" +
            "                     \"format\":\"Standard Thumbnail\",\n" +
            "                     \"height\":75,\n" +
            "                     \"width\":75\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15dc-immig-sub\\/merlin_154830909_a2ae6585-40ea-4aa6-91b7-d30898485f7f-mediumThreeByTwo210.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo210\",\n" +
            "                     \"height\":140,\n" +
            "                     \"width\":210\n" +
            "                  },\n" +
            "                  {  \n" +
            "                     \"url\":\"https:\\/\\/static01.nyt.com\\/images\\/2019\\/05\\/15\\/us\\/15dc-immig-sub\\/merlin_154830909_a2ae6585-40ea-4aa6-91b7-d30898485f7f-mediumThreeByTwo440.jpg\",\n" +
            "                     \"format\":\"mediumThreeByTwo440\",\n" +
            "                     \"height\":293,\n" +
            "                     \"width\":440\n" +
            "                  }\n" +
            "               ]\n" +
            "            }\n" +
            "         ],\n" +
            "         \"uri\":\"nyt:\\/\\/article\\/469444f5-e003-5af9-99a8-09ba5f542769\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";









    //IIIIIIIIIIIIIIIIIIIIIIIIIIIII














    String name, salary;
    TextView employeeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mostviewedarticles);




       //   toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setSubtitle("Most viewed Page");
    //   toolbar.inflateMenu(R.menu.menu_context);

       /*
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.citm4)
                {

                    String url = "https://www.nytimes.com/";

                    // Parse the URI and create the intent.
                    Uri webpage = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

                    // Find an activity to hand the intent and start that activity.
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    } else {
                        Log.d("ImplicitIntents", "Can't handle this intent!");
                    }

                }


                return false;
            }
        });

        */


        drawer = findViewById(R.id.drawer_layout);
        aw();


        // get the reference of TextView's
        employeeName = (TextView) findViewById(R.id.name2);
        String data ="";
        String dataParsed = "";
        String singleParsed ="";
        try {


            // get JSONObject from JSON file
            // JSONObject obj = new JSONObject(Json_string2);
            // fetch JSONObject named employee
            // JSONObject employee = obj.getJSONObject("results");
            // get employee name and salary
            //  name = employee.getString("list_name");
            // set employee name and salary in TextView's
            //  employeeName.setText("Name: " + name);



            JSONObject jsonObj = new JSONObject(JASON);
            String p = "";String p2 = " ";
            JSONArray c = jsonObj.getJSONArray("results");
            // Initializing list view with the custom adapter
            ArrayList<Item> itemList = new ArrayList<>();
            ArrayList<Item> itemList2 = new ArrayList<>();

            ItemArrayAdapter itemArrayAdapter = new ItemArrayAdapter(R.layout.list_item, itemList);

            recyclerView = (RecyclerView) findViewById(R.id.item_list);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

            recyclerView.setItemAnimator(new DefaultItemAnimator());

            recyclerView.setAdapter(itemArrayAdapter);

            for (int i = 1 ; i < c.length(); i++) {
                JSONObject obj = c.getJSONObject(i);
                String A = obj.getString("title");
                String B = obj.getString("source");




                p +=  " \ntitle: " + A + " Source: " +  B;
                itemList.add(new Item(p));

            }



            // Populating list items
           // for (int i = 0; i < 5; i++) {
           //     itemList.add(new Item("Item " + i));
           // }

            //employeeName.setText(p);


            //   employeeName.setText(p);
            // employeeSalary.setText(p2);



            //JSONArray JA = new JSONArray(Json_string2);
            //   for(int i =0 ;i <JA.length(); i++){
            //      JSONObject JO = (JSONObject) JA.get(i);
            //       singleParsed =  "Name:" + JO.get("list_name");


            //    dataParsed = dataParsed + singleParsed +"\n" ;


            //   }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        // employeeName.setText("H " + dataParsed);

    }


    private void aw() {

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();

        MenuItem gallery = menu.findItem(R.id.nav_gallery);
        MenuItem slideshow = menu.findItem(R.id.nav_slideshow);
        MenuItem tools = menu.findItem(R.id.nav_tools);
        MenuItem books = menu.findItem(R.id.bestbooks);


        // set new title to the MenuItem
        gallery.setTitle("Most Emailed Articles3");
        slideshow.setTitle("Most Shared Articles");
        tools.setTitle("Most Viewed Articles");
books.setTitle("BestBooks");

        MenuItem search = menu.findItem(R.id.share);

        search.setTitle("search books");

        navigationView.setNavigationItemSelectedListener(this);


    }



    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            // setTitle("#");
            //  first p = new first();
            // FragmentManager frag = getSupportFragmentManager();
            //   frag.beginTransaction().replace(R.id.fragment,p).commit();
            Intent intentMain = new Intent(mostviewedarticles.this,
                    MainActivity.class);
            mostviewedarticles.this.startActivity(intentMain);


        } else if (id == R.id.nav_gallery) {

            Intent intentMain = new Intent(mostviewedarticles.this,
                    MostSharedArticles.class);
            mostviewedarticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_slideshow) {
            Intent intentMain = new Intent(mostviewedarticles.this,
                    mostemailedarticles.class);
            mostviewedarticles.this.startActivity(intentMain);

        } else if (id == R.id.nav_tools) {
            Intent intentMain = new Intent(mostviewedarticles.this,
                    mostviewedarticles.class);
            mostviewedarticles.this.startActivity(intentMain);


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (id == R.id.bestbooks) {
            Intent intentMain = new Intent(mostviewedarticles.this,
                    bestBooks.class);
            mostviewedarticles.this.startActivity(intentMain);


        }
        else if (id == R.id.share) {
            Intent intentMain = new Intent(mostviewedarticles.this,
                    search.class);
            mostviewedarticles.this.startActivity(intentMain);


        }
        return true;

    }




}
